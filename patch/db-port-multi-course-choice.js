#!/usr/bin/env node

var dbus = require('../lib/data');
var util = require('../lib/util');
var _ = require('underscore');

function fa (uid, cb) {
    dbus.choice.chosen({
        uid: uid
    }, function (err, choice) {
        dbus.choice.choose({
            uid: uid,
            choice: choice
        }, cb);
    });
}

dbus.account.all({}, function (err, users) {
    util.amap(_(users).pluck('uid'), fa, function (err, result) {
        console.log(result);
    });
});

