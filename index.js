#!/usr/bin/env node

var _ = require('underscore');

// Configure default environment variables

_.defaults(process.env, {
    PORT: 10920,
    REDIS_PORT: 6379,
    REDIS_DB: 0,
    REDIS_SESSION_PORT: 6379,
    REDIS_SESSION_DB: 2,
});

// Start server

var server = require('./lib/server.js');

var PORT = parseInt(process.env.PORT);

server.listen(PORT);
console.log("Listen on " + PORT + ".");

