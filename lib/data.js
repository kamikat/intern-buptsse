
var _ = require('underscore');
var hash = require('node_hash');
var uuid = require('node-uuid');

var ApplicationError = require('./error').ApplicationError;

// Return Value Convention
//  Create  -> Created Data Item
//  Read    -> Target Data Item
//  Update  -> Updated Field & Field Value
//  Delete  -> Null

_(exports).extend({
    account: require('./model/account'),
    profile: require('./model/profile'),
    environment: require('./model/environment'),
    course: require('./model/course'),
    midware: require('./model/middleware'),
    task: require('./model/task'),
    choice: require('./model/choice'),
});

