
var _ = require('underscore');
var ApplicationError = require('./error').ApplicationError;

module.exports = function (args) {
    // {
    //  <Group>:<PhaseControl>,
    //  ...
    // }
    //
    // <PhaseControl>:<String>
    // '0,1,2,6-9' or '*'
    //
    // Extensive Group: `all'

    var m = _({
        student: '',
        professor: '',
        admin: '',
        all: ''
    }).extend(args);

    for (var key in m) {
        var expanded = m[key].replace(/[0-9]+-[0-9]+/gi, function (v) {
            var a = v.split('-');
            return _.range(parseInt(a[0]), parseInt(a[1]) + 1).join(',');
        });
        m[key] = _.chain(expanded.split(',')).compact().uniq().map(function (x) {
            return (x != '*') ? parseInt(x) : x;
        }).value();
    }

    for (var key in m) {
        if (key == 'all') continue;

        m[key] = _.union(m[key], m['all']);
    }

    return function (req, res, next) {
        // Access Control Logic

        var credit = req.session.credit;

        if (!credit) {
            return next(new ApplicationError({
                err: 'unauthorized',
                statuscode: 403
            }));
        }

        var group = req.session.credit.group;

        console.log(m, group);

        if (m[group].indexOf('*') != -1) return next();

        if (m[group].indexOf(req.env.phase) != -1) return next();

        return next(new ApplicationError({
            err: 'access_denied',
            statuscode: 403
        }));
    };
};

