
var hash = require('node_hash');
var URI = require('URIjs');

// Naming Convention
//  fragment := <fieldtype>.<qualifier>
//  key      := <fragment> | <key>:<fragment>

// Calculation
//  1:1 + 1:1 = 1:1
//  1:n + 1:1 = 1:n
//  1:1 + 1:n = 1:n
//  1:n + 1:n = 1:n
//  1:n + n:1 = n:n
//  n:n + *   = n:n

// XXX Entity
// "environ"                            Environment Parameters

var Env = exports.Env = {
    phase: Number,
    bulletin: String
};

// XXX Entity
// "user"                               All Users
// "user.<uid>"                         Authentication Information
// "user.<group>"                       All Users(group)

var User = exports.User = {
    uid: String,
    username: String,
    password: String,
    group: String
};

// XXX Entity
// "profile"                            All Profiles
// "profile.<pid>"                      Profile

var Profile = exports.Profile = {
    pid: String,
    active: Boolean,

    name: String,
    phone: String,
    email: String,
    photo: URI,
    resume: URI,

    last_modified: Date
};

// Optional Photo Attribute
exports.Profile.photo = function () {
        return "http://www.gravatar.com/avatar/" + hash.md5(this.email);
};

// XXX Entity
// "task"                               All Tasks
// "task.<tid>"                         Task

var TaskType = exports.TaskType = {
    SUBMISSION  : 'submission',
    NORMAL      : 'normal'
};

var Task = exports.Task = {
    tid: String,

    name: String,
    due: Date,
    deatil: String,

    last_modified: Date,

    type: TaskType,

    extension: {

        // When Task.type == 'submission'
        template: URI

    }
};

// XXX Entity
// "course"                             All Courses
// "course.<cid>"                       Course Information

var CourseType = exports.CourseType = {
    INTERN      : 'intern',
    CLASS       : 'class',
    LECTURE     : 'lecture'
};

var Course = exports.Course = {
    cid: String,

    name: String,
    demand: Number,
    detail: String,

    last_modified: Date,

    type: CourseType,

    extension: {

        // When Course.type == 'intern'
        company: {
            name: String,
            tutor: Profile,         // Quote Profile Structure
            website: URI,
            address: String,
            lonlat: String          // Optional
        },

    }
};

// XXX 1:1 User(X):Profile(Y)
// "user.<uid>:profile"                 User Profile
// "profile.<pid>:user"                 User of Profile

Profile.uid = User.uid;
User.pid = Profile.pid;

// XXX 1:1 Task(X):User(creator)
// "task.<tid>:user.creator"            Task Creator

Task.creator = User;

// XXX 1:n User(X):Task(creator)
// "user.<uid>:task.creator"            Task Created by User

User.created_task = [ Task ];

// XXX 1:n Course(X):Task+Course(X):Task(Y)
// "course.<cid>:task"                  Course Task List
// "course.<cid>:task.<tid>"            Course Task

Course.task = {
    tid: {
        task: Task,
        share: Number
    }
};

// XXX 1:1 Course(X):User(creator)
// "course.<cid>:user.creator"          Course Creator

Course.creator = User;

// XXX 1:n User(X):Course(creator)
// "user.<uid>:course.creator"          Course Created by User

User.created_course = [ Course ];

// XXX n:n Course(X):User(applier)+User(X):Course(applier)
// "course.<cid>:user.applier"          Course Applier List
// "user.<uid>:course.applier"          User Preferred Course List

Course.applier = [ User ];
User.applied_to = [ Course ];

// XXX n:n Course(X):User(admission)+User(X):Course(admission)
// "course.<cid>:user.admission"        Course Admitted User List
// "user.<uid>:course.admission"        User Admitted Course List

Course.admission = [ User ];
User.admitted_to = [ Course ];

// XXX 1:1 Course(X):User(Y)+User(X):Course(Y)
// "user.<uid>:course.<cid>"            User Content under Course
// "course.<cid>:user.<uid>"            User Content under Course

var CourseExecution = exports.CourseExecution = {
    cid: String,
    uid: String,

    task: {
        tid: {
            last_modified: Date,
            content: {
                // When Task.type == 'submission'
                submission: URI,
            },
            score: Number,          // Score of the work
        },
    },

    score: Number
};

