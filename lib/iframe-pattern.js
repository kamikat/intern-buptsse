
var jade = require('jade');

module.exports = jade.compile(
    'textarea(data-type="application/json") !{JSON.stringify(locals)}'
);


