
var _ = require('underscore');
var exp = require('express');
var crypto = require('crypto');
var con = require('console');
var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');
var csv = require('csv');
var hash = require('node_hash');
var app = exp();

var lessware = require('compile-mw-less');
var jadeware = require('compile-mw-jade-runtime');
var mdware = require('compile-mw-marked');
var iframepattern = require('./iframe-pattern');

var compile = {
    less: lessware({
        filename  : /(?:\/style\/)(.*)\.css/i,
        src       : 'www/less'
    }),
    jade: jadeware({
        filename  : /(?:\/ui\/)(.*)\.js/i,
        src       : 'www/ui',
        runtime   : /^\/js\/runtime((\.|-)min)?\.js$/i
    }),
    md: mdware({
        filename  : /(.*)\.md/i,
        src       : 'files'
    })
};

var error = require('./error');
var ApplicationError = error.ApplicationError;

var util = require('./util');

var dbus = require('./data');

var enable = require('./access');

app.configure(function () {
    app.set('views', path.resolve(__dirname, '../www/ui'))
    app.use(exp.logger());
    app.use(util.midware.plaintext);
    app.use(exp.bodyParser({
        keepExtensions: true,
        uploadDir: 'upload',
        limit: '4mb',
        hash: 'md5'
    }));
    app.use(util.midware.hashupload);
    app.use(util.midware.args);
    app.use(exp.cookieParser());
    app.use(exp.compress());
    app.use(exp.session({
        secret : crypto.randomBytes(16).toString('hex')
        // TODO Redis Session Store
        // store: new RedisStore({
        //      port: REDIS_SESSION_PORT,
        //      db: REDIS_SESSION_DB
        // })
    }));
    app.use(compile.less);
    app.use(compile.jade);
    app.use(compile.md);
    app.use(util.midware.routable(app._router));
    app.use(dbus.midware.environ);
    app.use(app.router);
    app.use(exp.static(path.resolve(__dirname, '../www/asset')));
    app.use(error.middleware);
});

app.get("/", function (req, res, next) {
    res.render("index.jade");
});

// Initialization completed

// Account {{{

app.get("/session", function (req, res, next) {
    if (!req.session.credit) {
        return res.send(200, {
            authenticated: false
        });
    } else {
        return res.send(req.session.credit);
    }
});

app.post("/session", function (req, res, next) {
    var args = req.args;

    dbus.account.verify({
        username: args.username,
        password: args.password
    }, function (err, user) {
        if (err) return next(err);

        _(user).extend({
            authenticated: true
        });

        req.session.credit = user;

        return res.send(user);
    });
});

app.delete("/session", function (req, res, next) {
    req.session.destroy();
    return res.send({ authenticated: false });
});

app.post("/passwd", enable({
    all: '*'
}), function (req, res, next) {
    var args = req.args;
    var credit = req.session.credit;

    // 2nd Auth
    dbus.account.verify({
        uid: credit.uid,
        password: args.oldpass
    }, function (err, user) {
        return next(err);
    });
}, function (req, res, next) {
    var args = req.args;
    var credit = req.session.credit;

    dbus.account.update({
        uid: credit.uid,
        password: args.newpass
    }, function (err, user) {
        if (err) return next(err);

        _(user).extend({
            authenticated: true
        });

        return res.send(user);
    });
});

app.get("/account/", enable({
    admin: '*'
}), function (req, res, next) {
    dbus.account.all({}, function (err, users) {
        if (err) return next(err);

        // Fetch Profile for each User
        util.amap(users, function (user, cb) {
            dbus.profile.get(user, function (err, profile) {
                if (err) return cb(err);

                user.profile = profile;

                return cb(user);
            });
        }, function (err, users) {
            if (err) return next(err);

            return res.send(users);
        });
    });
});

app.delete("/account/:uid", enable({
    admin: '*'
}), function (req, res, next) {
    dbus.account.delete({
        uid: req.params.uid
    }, function (err) {
        if (err) return next(err);

        return res.send(200);
    });
});

app.post("/account/", enable({
    admin: '*'
}), function (req, res, next) {
    var args = req.args;

    dbus.account.create({
        username: args.username,
        password: args.password,
        group: args.group
    }, function (err, user) {
        if (err) return next(err);

        return res.send(user);
    });
});

app.post("/account/!import", enable({
    admin: '*'
}), function (req, res, next) {
    var file = req.files['import'];

    var reader = csv();

    var header;
    var stat = res.stat = {
        count: 0,
        created: 0,
        failed: []
    };

    var queue = 0;
    var valid = false;

    var feedback = function (err, row, index) {

        stat.count++;

        if (err) {
            var err_body = {
                index: index,
                username: row.username,
                name: row.name
            }
            err_body.err = {
                'username_exists': 'duplicated'
            }[err.body.err] || 'unknown';
            stat.failed.push(err_body);
        } else {
            stat.created++;
        }

        queue--;
        if (queue == 0) {
            return next();
        }
    };

    reader
    .from.path(file.path)
    .on('record', function (row, index) {
        if (index == 0) {
            header = _(row).map(function (x) { return x.toLowerCase() });
            if (!_.all(['username', 'password', 'group', 'name'], function (k) {
                return ~header.indexOf(k);
            })) {
                return next(new ApplicationError({
                    err: 'invalid_format',
                    transformer: iframepattern
                }));
            } else {
                valid = true;
            }
        } else if (valid) {
            queue++;
            var _user = _.object(header, row);
            dbus.account.create({
                username: _user.username,
                password: hash.md5(_user.password),
                group: _user.group.toLowerCase()
            }, function (err, user) {
                if (err) return feedback(err, _user, index);

                dbus.profile.set(_.extend({
                    uid: user.uid
                }, _user), function (err, patch) {
                    return feedback(err, _user, index);
                });
            });
        }
    })
    .on('end', function (count) {
        if (queue == 0 && valid) {
            // Empty CSV File is Uploaded
            return next();
        }
    })
    .on('error', function (err) {
        return next(new ApplicationError({
            err: 'invalid_format',
            transformer: iframepattern
        }));
    });
}, function (req, res, callback) {
    return res.send(iframepattern(res.stat));
});

app.post("/account/:uid", enable({
    admin: '*'
}), function (req, res, next) {
    var args = req.args;
    args.uid = req.params.uid;

    // Here, administrator can change the password of any user
    // without need of verifying the old password.

    dbus.account.update(req.args, function (err, result) {
        if (err) return next(err);

        return res.send(result);
    });
});

// }}}

// Environment {{{

app.get("/environment", function (req, res, next) {
    return res.send(req.env);
});

app.post("/environment", enable({
    admin: '*'
}), function (req, res, next) {
    var args = req.args;

    var uid = req.session.credit.uid;
    var password = args.password || '';

    if (args.phase) {
        // Need 2rd auth to change phase
        dbus.account.verify({
            uid: uid,
            password: password
        }, function (err, user) {
            if (err) return next(err);

            return next();
        });
    } else {
        return next();
    }
}, function (req, res, next) {
    dbus.environment.set(req.args, function (err, env) {
        if (err) return next(err);

        req.env = env;

        return res.send(env);
    });
});

// }}}

// Profile {{{

app.get("/u/:uid/profile", enable({
    all: '*'
}), function (req, res, next) {
    var params = req.params;

    dbus.profile.get({
        uid: params.uid
    }, function (err, profile) {
        if (err) return next(err);

        return res.send(profile);
    });
});

app.post("/u/:uid/profile", enable({
    all: '*'
}), function (req, res, next) {
    var params = req.params;
    var credit = req.session.credit;

    if (params.uid != credit.uid && credit.group != 'admin') {
        return next(new ApplicationError({
            err: 'permission_denied'
        }));
    }

    var args = req.args;

    args.uid = params.uid;

    dbus.profile.set(args, function (err, profile) {
        if (err) return next(err);

        return res.send(profile);
    });
});

// }}}

// Course {{{

app.get("/course/", enable({
    admin: '*',
    professor: '*',
    student: '1-2'
}), function (req, res, next) {
    var credit = req.session.credit;

    var args = {};

    // Show User's Course to professor, all Courses to the others
    if ('professor' == credit.group) {
        args.uid = credit.uid;
    }

    dbus.course.all(args, function (err, courses) {
        if (err) return next(err);

        return res.send(courses);
    });
});

app.post("/course/", enable({
    admin: '1-4',
    professor: '1-3'
}), function (req, res, next) {
    var args = req.args;
    var credit = req.session.credit;

    _(args).extend({
        uid: credit.uid
    });

    dbus.course.create(args, function (err, course) {
        if (err) return next(err);

        return res.send(course);
    });
});

app.post("/course/:id", enable({
    admin: '*',
    professor: '1-3'
}), function (req, res, next) {
    var args = req.args;
    var credit = req.session.credit;

    // TODO Check Ownership of a Course

    _(args).extend({
        uid: credit.uid,
        id: req.params.id
    });

    dbus.course.update(args, function (err, course) {
        if (err) return next(err);

        return res.send(course);
    });
});

app.delete("/course/:id", enable({
    admin: '1-4',
    professor: '1-3'
}), function (req, res, next) {
    var credit = req.session.credit;

    // TODO Check Ownership of a Course

    dbus.course.delete({
        uid: credit.uid,
        id: req.params.id
    }, function (err) {
        if (err) return next(err);

        return res.send(200);
    });
});

// }}}

// Files {{{

app.post("/files/:uid/", enable({
    all: '*'
}), function (req, res, next) {

    var params = req.params;
    var credit = req.session.credit;

    if (params.uid != credit.uid && credit.group != 'admin') {
        return next(new ApplicationError({
            err: 'permission_denied'
        }));
    }

    util.amap(_.keys(req.files), function (k, cb) {
        var f = req.files[k];
        var tmppath = f.path;
        var targetpath = path.resolve(__dirname, '../files',
                                     req.session.credit.uid, f.name);
        mkdirp(path.dirname(targetpath), function(err) {
            if (err) return next(err);

            fs.rename(tmppath, targetpath, function(err) {
                if (err) return next(err);

                return cb(null, '/files/' + credit.uid + '/' + f.name);
            });
        });
    }, function(err, result) {
        if (err) return next(err);

        return res.send(iframepattern(_.object(_.keys(req.files), result)));
    });
});

app.get("/files/:filename", enable({
    all: '*'
}), function (req, res, next) {
    var filename = req.params.filename;
    res.download(path.resolve(__dirname, '../files', filename), filename);
});

app.get("/files/:uid/:filename", enable({
    all: '*'
}), function (req, res, next) {
    var uid = req.params.uid;
    var filename = req.params.filename;
    res.download(path.resolve(__dirname, '../files', uid, filename),
                uid + '-' + filename);
});

app.delete('/files/:uid/:filename', enable({
    all: '*'
}), function(req, res, next) {
    var params = req.params;
    var credit = req.session.credit;

    if (params.uid != credit.uid && credit.group != 'admin') {
        return next(new ApplicationError({
            err: 'permission_denied'
        }));
    }

    var pathname = path.resolve(__dirname, '../files', params.uid,
                                req.params.filename);

    fs.unlink(pathname, function(err) {
        if (err) return next(err);

        return res.send({});
    });
});

// }}}

// FAQ {{{

app.get("/faq", enable({
    admin: '*'
}), function (req, res, next) {
    fs.readFile(path.resolve(__dirname, "../files/faq.md"), function (err, data) {
        if (err) return next(err);

        res.setHeader('Content-Type', 'text/plain');

        return res.send(data);
    });
});

app.post("/faq", enable({
    admin: '*'
}), function (req, res ,next) {
    fs.writeFile(path.resolve(__dirname, "../files/faq.md"), req.text, function (err, data) {
        if (err) return next(err);

        return res.send(data);
    });
});

// }}}

// Task {{{

app.get("/task/", enable({
    admin: '*',
    professor: '*',
    student: '*'
}), function (req, res, next) {
    dbus.task.all(function (err, tasks) {
        if (err) return next(err);

        return res.send(tasks);
    });
});


app.post("/task/", enable({
    admin: '*',
}), function (req, res, next) {
    dbus.task.create(req.args, function (err, task) {
        if (err) return next(err);

        return res.send(task);
    });
});


app.post("/task/:id", enable({
    admin: '*'
}), function (req, res, next) {
    var args = req.args;
    var credit = req.session.credit;

    _(args).extend({
        uid: credit.uid,
        id: req.params.id
    });

    dbus.task.update(args, function (err, task) {
        if (err) return next(err);

        return res.send(task);
    });
})


app.delete("/task/:id", enable({
    admin: '*',
}), function (req, res, next) {
    var args = req.args;
    var credit = req.session.credit;

    _(args).extend({
        uid: credit.uid,
        id: req.params.id
    });

    dbus.task.delete(args, function (err) {
        if (err) return next(err);
        return res.send(200);
    });
})

// }}}

// Choice {{{

app.get("/u/:uid/choice", enable({
    admin: "*",
    professor: "*",
    student: "*",
}), function (req, res, next) {
    var args = req.args;
    var uid = req.params.uid;

    _(args).extend({
        uid: uid
    });

    dbus.choice.chosen(args, function (err, choice) {
        if (err) return next(err);

        return res.send(choice);
    });
});

app.post("/u/:uid/choice", enable({
    admin: "*",
    professor: "*",
    student: "*",
}), function (req, res, next) {
    var args = req.args;
    var uid = req.params.uid;

    _(args).extend({
        uid: uid
    });

    dbus.choice.choose(args, function (err, choice) {
        if (err) return next(err);

        return res.send(choice);
    });
});

app.get("/course/:id/choice", enable({
    admin: "*",
    professor: "*"
}), function (req, res, next) {
    var args = req.args;
    var id = req.params.id;

    _(args).extend({
        id: id
    });

    dbus.choice.chooser(args, function (err, chooser) {
        if (err) return next(err);

        return res.send(chooser);
    });
});

// }}}

module.exports = app;

