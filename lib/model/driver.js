
var _ = require('underscore');
var redis = require('redis');

var REDIS_PORT = parseInt(process.env.REDIS_PORT);
var REDIS_DB = parseInt(process.env.REDIS_DB);

var db = redis.createClient({
    port: REDIS_PORT
});

db.select(REDIS_DB);

var DatabaseError = require('../error').DatabaseError;

var _send_command = db.send_command;

db.send_command = function (command, args, _cb) {
    if (!_cb && Array.isArray(args)) {
        var last_arg_type = typeof args[args.length - 1];
        if (last_arg_type === 'function' ||
            last_arg_type === 'undefined') {
            _cb = args.pop();
        }
    }
    var cb = _cb && function (err, result) {
        if (err) return _cb(new DatabaseError(err));
        return _cb(err, result);
    };
    return _send_command.apply(this, [ command, args, cb ]);
};

db.hmsetnx = function (key, obj, cb) {
    // Set NX with Expiration in 30s
    db.set(key, 'Taken', 'NX', 'EX', 30, function (err, result) {
        if (err) return cb(err);

        if (result) {
            db.multi()
            .del(key)
            .hmset(key, obj)
            .exec(function (err, result) {
                if (err) return cb(err);

                return cb(null, 1);
            });
        } else {
            return cb(null, 0);
        }
    });
};

module.exports = db;

