
var _ = require('underscore');

var db = require('./driver');

var define = require('./converter');

var model = define({
    active  : define.boolean,

    name    : define.string,
    phone   : define.string,
    email   : define.string,
    photo   : define.string,
    resume  : define.string,

    last_modified: define.datetime,
});

_(exports).extend({

    get: function (args, cb) {

        var uid = args.uid;

        db.hgetall('profile:' + uid, function (err, profile) {
            if (err) return cb(err);

            // Restore Model Object from stored Redis Hash
            // (filtered, with defaults)
            return cb(null, model.load(profile));
        });

    },

    set: function (args, cb) {

        var uid = args.uid;

        args.last_modified = Date.now();

        exports.get({
            uid: uid
        }, function (err, profile) {
            if (err) return cb(err);

            // Filter input args to match Model
            var patch = model.filter(args);

            // Check Active
            patch.active = _([
                            'name',
                            'phone',
                            'email'
            ]).all(function (x) {
                // Handle case when reset field in patch
                if (patch.hasOwnProperty(x)) {
                    return patch[x];
                } else {
                    return profile[x];
                }
            });

            // Make Javascript Object match Model definition
            var store = model.store(patch);

            db.hmset('profile:' + uid, store, function (err, result) {
                if (err) return cb(err);

                return cb(null, patch);
            });
        });

    },

});

