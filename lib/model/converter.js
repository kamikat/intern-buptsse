
var _ = require('underscore');

var converter = function (model) {

    var facade = {
        filter: function (val) {
            return facade.load(facade.store(val), true);
        },
        publish: function (val) {
            return _(val).pick(_(val).chain().keys().filter(function (k) {
                return (k[0] != '_');
            }).value());
        },
    };

    if (Array.isArray(model)) {
        _(facade).extend({
            store: function (val, defaults) {
                if (defaults) {
                    return model[1](val);
                } else {
                    // Store existing field only
                    return val && model[1](val);
                }
            },
            load: function (val, nodefaults) {
                if (nodefaults) {
                    return val && model[0](val);
                } else {
                    // Load non-exists field by default value
                    return model[0](val);
                }
            },
        });
    } else {
        var m = _(model);
        _(facade).extend({
            store: function (val, defaults) {
                var ret = {};
                val = ('object' == typeof val) && val || {};
                m.each(function (v, k) {
                    if (defaults) {
                        ret[k] = v[1](val[k]);
                    } else {
                        // Store existing field only
                        if (val[k] !== undefined) {
                            ret[k] = v[1](val[k]);
                        }
                    }
                });
                return ret;
            },
            load: function (val, nodefaults) {
                var ret = {};
                val = ('object' == typeof val) && val || {};
                m.each(function (v, k) {
                    if (nodefaults) {
                        if (val[k] !== undefined) {
                            ret[k] = v[0](val[k]);
                        }
                    } else {
                        // Load non-exists field by default value
                        ret[k] = v[0](val[k]);
                    }
                });
                return ret;
            },
        });
    }

    return facade;
};

// Converter Definition:
//  [0] for `load'  operator
//  [1] for `store' operator

// Basic Data Types

_(converter).extend({
    boolean : [
        function (val) { return val == 'true' },
        function (val) { return '' + val },
    ],
    number  : [
        function (val) { return parseInt(val || 0) },
        function (val) { return '' + (val || 0) },
    ],
    string  : [
        function (val) { return val && "" + val || '' },
        function (val) { return val && "" + val || '' },
    ],
    datetime: [
        function (val) { return parseInt(val || 0) },
        function (val) { return '' + (val || Date.now()) },
    ],
    object  : [
        function (val) { return JSON.parse(val || '{}') },
        function (val) { return JSON.stringify(val || {}) },
    ],
    array   : [
        function (val) { return JSON.parse(val || '[]') },
        function (val) { return JSON.stringify(val || []) },
    ],
});

// Extended Types

var passwordhash = require('password-hash');
var shortid = require('shortid');

_(converter).extend({
    password: [
        function (val) {
            // Define export as a verifier
            var verifier = function (password) {
                return passwordhash.verify(password, val);
            };
            verifier.password = val;
            return verifier;
        },
        function (val) {
            if (val instanceof Function) {
                // When with an already existed password
                if (!val.password) {
                    throw new Error("Unable to store password field, invalid input");
                }
                return val.password;
            }
            return passwordhash.generate('' + val);
        },
    ],
    uuid: [
        function (val) {
            return val || '';
        },
        function (val) {
            return val || shortid.generate();
        }
    ],
});

module.exports = converter;

