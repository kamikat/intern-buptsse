
var _ = require('underscore');

var env = require('./environment');

_(exports).extend({
    environ: function (req, res, next) {
        // Dependency: util.midware.routable
        if (!req.routable) return next();
        env.get({}, function (err, env) {
            if (err) return next(err);

            req.env = env;

            return next();
        });
    },
});
