
var _ = require('underscore');
var util = require('../util');

var db = require('./driver');

var define = require('./converter');

var model = define({
    id      : define.uuid,
    name    : define.string,
    department: define.string,
    demand  : define.number,
    salary  : define.string,
    brief   : define.string,
    mentor  : define.object,
    enterprise: define.object,
    task    : define.array,
    creator : define.string,
    created_at: define.datetime,
});

var ApplicationError = require('../error').ApplicationError;

_(exports).extend({

    create: function (args, cb) {

        var course = model.filter(args);

        course.creator = args.uid;

        var store = model.store(course, true);

        var fail = 0;

        db.hmsetnx('course:' + store.id, store, function (err, result) {
            if (err) return cb(err);

            if (result) {

                // Register to User's course
                db.sadd('user:' + args.uid + ':course', store.id);

                // Register to Global course
                db.sadd('course', store.id);

                return cb(null, model.load(store));

            } else {

                if (++fail > 3) {
                    // It's an unfortunate error
                    return cb(new ApplicationError({
                        err: 'bad_luck',
                    }));
                } else {
                    // Retry when id collision occurred
                    store = model.store(course, true);
                    db.hmsetnx('course:' + store.id, store, arguments.callee);
                }

            }
        });

    },

    all: function (args, cb) {

        var query = args.uid && 'user:' + args.uid + ':course' || 'course';

        db.smembers(
            args.uid && 'user:' + args.uid + ':course' ||
            'course', function (err, courses) {
            if (err) return cb(err);

            util.amap(courses || [], function (id, cb) {
                db.hgetall('course:' + id, function (err, result) {
                    if (err) return cb(err);

                    return cb(null, model.load(result));
                });
            }, function (err, courses) {
                if (err) return cb(err);

                return cb(null, courses);
            });
        });

    },

    update: function (args, cb) {

        var patch = model.filter(args);

        var store = model.store(patch);

        db.hmset('course:' + patch.id, store, function (err, result) {
            if (err) return cb(err);

            return cb(null, patch);
        });

    },

    delete: function (args, cb) {

        db.srem('user:' + args.uid + ':course', args.id);

        db.srem('course', args.id);

        db.del('course:' + args.id, function (err, result) {
            if (err) return cb(err);

            if (!result) {
                return cb(new ApplicationError({
                    err: 'invalid_course',
                }));
            } else {
                return cb();
            }
        });

    },

});

