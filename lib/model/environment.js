
var _ = require('underscore');

var db = require('./driver');

var define = require('./converter');

var model = define({
    phase   : define.number,
    bulletin: define.string,
    schedule: define.string
});

_(exports).extend({

    get: function (args, cb) {

        db.hgetall('environ', function (err, env) {
            if (err) return cb(err);

            // Restore patch to Javascript Object
            // (filtered, with defaults)
            return cb(null, model.load(env));
        });

    },

    set: function (args, cb) {

        // Make Javascript Object match Model definition
        var store = model.store(args);

        db.hmset('environ', store, function (err, result) {
            if (err) return cb(err);

            // Restore patch to Javascript Object (filtered)
            // NOTE The second parameter `nodefaults` indicates not
            // replace undefined field with default value
            return cb(null, model.load(store, true));
        });

    },

});

