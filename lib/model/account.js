
var _ = require('underscore');
var util = require('../util');

var db = require('./driver');

var define = require('./converter');

var model = define({
    username: define.string,
    password: define.password,

    uid     : define.string,
    group   : define.string,

    created_at: define.datetime,
});

var ApplicationError = require('../error').ApplicationError;

_(exports).extend({

    create: function (args, cb) {

        var user = model.filter(args);

        // Find uid according to username
        var uid = user.uid = user.username;

        // Convert to Object friendly to Redis Hash
        //  user.password should be salted hashed according to define.password
        //  operator
        var store = model.store(user);

        db.hmsetnx('user:' + uid, store, function (err, result) {
            if (err) return cb(err);

            if (result) {
                // Created Successfully

                // Register to User Set
                db.sadd('user', uid);

                return cb(null, user);
            } else {
                // Exists
                return cb(new ApplicationError({
                    err: 'username_exists',
                }));
            }
        });

    },

    verify: function (args, cb) {

        var username = args.username;
        var password = args.password;

        // Find uid according to username
        var uid = args.uid || username;

        db.hgetall('user:' + uid, function (err, result) {
            if (err) return cb(err);

            if (!result) {
                // Invalid User
                return cb(new ApplicationError({
                    err: 'invalid_account'
                }));
            } else {
                // Convert Redis Object to JavaScript Object
                // (filtered, with defaults)
                var user = model.load(result);

                // `model.load' will generate parse as verifying function
                if (!user.password(password)) {
                    // Invalid Password
                    return cb(new ApplicationError({
                        err: 'invalid_account'
                    }));
                } else {
                    return cb(null, user);
                }
            }
        });

    },

    update: function (args, cb) {

        var uid = args.uid;

        var patch = model.filter(args);

        // If password request, store will automatically generate password
        var store = model.store(patch);

        db.hmset('user:' + uid, store, function (err, user) {
            if (err) return cb(err);

            return cb(null, patch);
        });

    },

    all: function (args, cb) {

        db.smembers('user', function (err, users) {
            if (err) return cb(err);

            util.amap(users || [], function (uid, cb) {
                db.hgetall('user:' + uid, function (err, result) {
                    if (err) return cb(err);

                    return cb(null, model.load(result));
                });
            }, function (err, users) {
                if (err) return cb(err);

                return cb(null, users);
            });
        });

    },

    delete: function (args, cb) {

        var uid = args.uid;

        db.srem('user', uid);

        db.del('user:' + uid, function (err, result) {
            if (err) return cb(err);

            if (!result) {
                return cb(new ApplicationError({
                    err: 'invalid_user'
                }));
            } else {
                return cb();
            }
        });

    },

});

// Administrator Account Initialization {{{

var colors = require('colors');

var hash = require('node_hash');

var DEFAULT_ADMIN = "admin";

db.scard('user', function (err, result) {
    if (!result) {
        var username = DEFAULT_ADMIN;
        var password = util.randomString(12);
        exports.create({
            username: username,
            password: hash.md5(password),
            group: 'admin'
        }, function (err, result) {
            if (err) {
                // Exit on database error
                if (err.code == 'EDBERR') {
                    console.log("[" + "ERROR".red + "] " +
                                "Initialization Failed, cannot create administrator account due to database error.");
                    process.exit(1);
                }
            }
            console.log("[" + "INFO".green + "] " +
                        "Initialized administrator account " + username.bold + " with password " + password.bold);
            console.log("[" + "INFO".green + "] " +
                        "Please login and change your password as soon as possible");
        });
    }
});

// }}}

