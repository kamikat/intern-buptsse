
var _ = require('underscore');
var util = require('../util');

var db = require('./driver');

var define = require('./converter');

var model = define({
    id          : define.uuid,
    name        : define.string,
    type        : define.string,
    asset       : define.object,
    share       : define.number,
    due_date    : define.datetime,
    creator     : define.string,
    create_at   : define.datetime
});

var ApplicationError = require('../error').ApplicationError;

_(exports).extend({

    create : function (args, cb) {

        var task = model.filter(args);

        task.creator = args.uid;

        var store = model.store(task, true);

        var fail = 0;

        db.hmsetnx('task:' + store.id, store, function (err, result) {

            if (err) return cb(err);

            if (result) {
                // Register to Tasks
                db.sadd('task', store.id);

                return cb(null, model.load(store));
            } else {

                if (++fail > 3) {
                    // Seems I'm an unlucky dog
                    return cb(new ApplicationError({
                        err: 'bad_luck',
                    }));
                } else {
                    // Retry
                    store = model.store(task, true);
                    db.hmsetnx('task:' + store.id, store, argument.callee);
                }
            }

        });
    },

    all: function (cb) {
        db.smembers('task', function(err, tasks) {
            if (err) return cb(err);

            util.amap(tasks || [], function (id, cb) {
                db.hgetall('task:' + id, function(err, result) {
                    if (err) return cb(err);

                    return cb(null, model.load(result));
                })
            }, function(err, tasks) {
                if (err) return cb(err);

                return cb(null, tasks);
            });
        });
    },

    update: function (args, cb) {

        var patch = model.filter(args);

        var store = model.store(patch);

        db.hmset('task:' + store.id, store, function (err, result) {
            if (err) return cb(err);

            return cb(null, patch);
        });

    },

    delete: function (args, cb) {
        db.srem('task', args.id);
        db.del('task:' + args.id, function(err, result) {
            if (err) return cb(err);

            if (!result) {
                return cb(new ApplicationError({
                    err: 'invalid_task',
                }));
            } else {
                return cb();
            }
        });
    }

});
