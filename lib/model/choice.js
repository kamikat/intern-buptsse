
var _ = require('underscore');
var util = require('../util');

var db = require('./driver');

var define = require('./converter');

var model = define(define.array);

var ApplicationError = require('../error').ApplicationError;

_(exports).extend({

    ///
    // Choose Courses for User
    //
    choose: function (args, cb) {

        var uid = args.uid;

        var store = model.store(args.choice, true);
        var _choice = model.load(store);

        db.get('user:' + uid + ':choice', function (err, choice) {
            if (err) return cb(err);

            choice = model.load(choice);

            // Unset all choices
            util.amap(choice, function (choice, cb) {
                db.multi()
                .srem('course:' + choice + ':choice', uid)
                .srem('course:' + choice + ':choice:first', uid)
                .srem('course:' + choice + ':choice:second', uid)
                .srem('course:' + choice + ':choice:third', uid)
                .exec(cb);
            }, function (err, result) {

                db.set('user:' + uid + ':choice', store, function (err, result) {
                    if (err) return cb(err);

                    util.amap(_choice, function (choice, cb) {
                        db.multi()
                        .sadd('course:' + choice + ':choice', uid)
                        .sadd('course:' + choice + ':choice:' + ['first', 'second', 'third'][_choice.indexOf(choice)], uid)
                        .exec(cb);
                    }, function (err, result) {
                        if (err) return cb(err);

                        // Return with Nothing
                        return cb(null, {
                            uid: uid,
                            choice: _choice
                        });
                    });
                });

            });
        });

    },

    ///
    // Get Choosers of a Course
    //
    chooser: function (args, cb) {
        var id = args.id;

        var nths = ['first', 'second', 'third'];

        util.amap(nths, function (nth, cb) {
            db.smembers('course:' + id + ':choice:' + nth, cb);
        }, function (err, result) {
            if (err) return cb(err);

            var ret = _(result).map(function (x) { return x || []; });

            return cb(null, _.object(nths, ret));
        });
    },

    ///
    // Get Course chosen by User
    //
    chosen: function (args, cb) {
        var uid = args.uid;

        db.get('user:' + uid + ':choice', function (err, result) {
            if (err) return cb(err);

            return cb(null, model.load(result));
        });
    },

});

