
var util = require('util');

function ApplicationError(options) {
    if (typeof options == 'string') {
        options = { message: options };
    }

    this.name = 'ApplicationError';
    this.code = 'EAPPERR';
    this.message = options.message || "Application error";
    Error.captureStackTrace(this, options.stackStartFunction || ApplicationError);

    this.statuscode = options.statuscode || 200;

    // Body Transformer
    this.transformer = options.transformer || function (body) { return body };

    delete options.message;
    delete options.stackStartFunction;
    delete options.statuscode;
    delete options.transformer;

    this.body = options;
}

function DatabaseError(options) {
    if (typeof options == 'string') {
        options = { message: options };
    }

    this.name = 'DatabaseError';
    this.code = 'EDBERR';
    this.message = options.message || "Database error";
    Error.captureStackTrace(this, options.stackStartFunction || ApplicationError);

    this.statuscode = options.statuscode || 500;

    delete options.message;
    delete options.stackStartFunction;
    delete options.statuscode;

    this.body = options;

    this.body.err = this.body.err || 'database_failed';
}

util.inherits(ApplicationError, Error);
util.inherits(DatabaseError, Error);

module.exports = {
    ApplicationError: ApplicationError,
    DatabaseError: DatabaseError,
    middleware: function (err, req, res, next) {
        if (err instanceof ApplicationError ||
            err instanceof DatabaseError) {
            return res.send(err.statuscode, err.transformer(err.body));
        }

        return next(err);
    }
};

