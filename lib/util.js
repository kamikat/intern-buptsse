
var _ = require('underscore');

var path = require('path');
var fs = require('fs');

var bufferhelper = require('bufferhelper');

function amap (keys, proc, done) {
    if(!keys || !keys.length) return done(null, []);

    var counter = 0;
    var result = keys.slice(0);

    keys.forEach(function (key, index) {
        counter++;
        process.nextTick(proc.bind(null, key, function (err, v) {
            counter--;
            result[index] = v || err;
            if(counter == 0)
                return done(null, result);
        }));
    });
};

exports.amap = amap;

exports.randomString = function (len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
};

exports.md5quote = (function () {
    var qcache = {};
    return function (uid) {
        var cache = qcache[uid];
        if (cache) return cache;
        return qcache[uid] = hash.md5(uid);
    };
})();

// Utility Middleware

exports.midware = {};

_(exports.midware).extend({
    args: function (req, res, next) {
        if (req.method == 'GET') {
            req.args = req.query;
        } else if (req.method == 'POST') {
            req.args = req.body;
        } else {
            req.args = {};
        }

        res.charset = 'utf-8';

        return next();
    },
    hashupload: function (req, res, next) {
        if(!req.files) return next();

        var files = [];
        for (var name in req.files) {
            var file = req.files[name];
            if (Array.isArray(file)) {
                files = files.concat(files, file);
            } else {
                files.push(file);
            }
        }

        if(!files.length) return next();

        amap(files, function (file, done) {
            if(!file.hash) return done();
            var newPath = path.join(
                path.dirname(file.path),
                file.hash + path.extname(file.path)
            );
            fs.exists(newPath, function(exists) {
                if (exists) {
                    fs.unlink(file.path, done);
                } else {
                    fs.rename(file.path, newPath, done);
                }
                file.path = newPath;
            });
        }, next);
    },
    plaintext: function (req, res, next) {
        if (req.is('text/*')) {
            req.setEncoding('utf8');
            new bufferhelper().load(req, function (err, buffer) {
                req.text = buffer.toString();
                return next();
            });
        } else {
            next();
        }
    },
    routable: function (approuter) {
        var cache = {};
        return function (req, res, next) {
            if (cache.hasOwnProperty(req.path)) {
                req.routable = cache[req.path];
            } else {
                var route = approuter.match(req.method.toLowerCase(), req.path, 0)
                req.routable = cache[req.path] = route !== undefined;
            }
            return next();
        };
    }
});

