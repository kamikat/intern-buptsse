#!/bin/sh

CWD=`pwd`

if [ ! -x $CWD/bin/redis-server ]; then

    wget http://redis.googlecode.com/files/redis-2.6.14.tar.gz
    tar xzf redis-2.6.14.tar.gz
    cd redis-2.6.14

    PREFIX=$CWD make install

    rm $CWD/redis-2.6.14.tar.gz
    rm -r $CWD/redis-2.6.14

fi

exec $CWD/bin/redis-server redis.conf

