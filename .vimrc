set expandtab
set ts=4
set sw=4
set st=4

" Auto strip useless space when file is saved
au BufWritePre * :call <SID>StripTrailingWhitespace()

fun! <SID>StripTrailingWhitespace()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l,c)
endfun

au FileType javascript setlocal textwidth=78
set fdm=marker

au FileType html,jade,markdown setlocal ts=2 sw=2

