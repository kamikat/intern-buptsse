//////////////
// IE Shims //
//////////////

// See https://gist.github.com/527683

var ie = (function(){

    var undef,
    v = 3,
    div = document.createElement('div'),
    all = div.getElementsByTagName('i');

    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]
    );

    return v > 4 ? v : undef;

}());

// See https://gist.github.com/dhm116/1790197

// Add ECMA262-5 method binding if not supported natively
//
if (!('bind' in Function.prototype)) {
    Function.prototype.bind= function(owner) {
        var that= this;
        if (arguments.length<=1) {
            return function() {
                return that.apply(owner, arguments);
            };
        } else {
            var args= Array.prototype.slice.call(arguments, 1);
            return function() {
                return that.apply(owner, arguments.length===0? args : args.concat(Array.prototype.slice.call(arguments)));
            };
        }
    };
}

// Add ECMA262-5 string trim if not supported natively
//
if (!('trim' in String.prototype)) {
    String.prototype.trim= function() {
        return this.replace(/^\s+/, '').replace(/\s+$/, '');
    };
}

// Add ECMA262-5 Array methods if not supported natively
//
if (!('indexOf' in Array.prototype)) {
    Array.prototype.indexOf= function(find, i /*opt*/) {
        if (i===undefined) i= 0;
        if (i<0) i+= this.length;
        if (i<0) i= 0;
        for (var n= this.length; i<n; i++)
            if (i in this && this[i]===find)
                return i;
        return -1;
    };
}
if (!('lastIndexOf' in Array.prototype)) {
    Array.prototype.lastIndexOf= function(find, i /*opt*/) {
        if (i===undefined) i= this.length-1;
        if (i<0) i+= this.length;
        if (i>this.length-1) i= this.length-1;
        for (i++; i-->0;) /* i++ because from-argument is sadly inclusive */
            if (i in this && this[i]===find)
                return i;
        return -1;
    };
}
if (!('forEach' in Array.prototype)) {
    Array.prototype.forEach= function(action, that /*opt*/) {
        for (var i= 0, n= this.length; i<n; i++)
            if (i in this)
                action.call(that, this[i], i, this);
    };
}
if (!('map' in Array.prototype)) {
    Array.prototype.map= function(mapper, that /*opt*/) {
        var other= new Array(this.length);
        for (var i= 0, n= this.length; i<n; i++)
            if (i in this)
                other[i]= mapper.call(that, this[i], i, this);
        return other;
    };
}
if (!('filter' in Array.prototype)) {
    Array.prototype.filter= function(filter, that /*opt*/) {
        var other= [], v;
        for (var i=0, n= this.length; i<n; i++)
            if (i in this && filter.call(that, v= this[i], i, this))
                other.push(v);
        return other;
    };
}
if (!('every' in Array.prototype)) {
    Array.prototype.every= function(tester, that /*opt*/) {
        for (var i= 0, n= this.length; i<n; i++)
            if (i in this && !tester.call(that, this[i], i, this))
                return false;
        return true;
    };
}
if (!('some' in Array.prototype)) {
    Array.prototype.some= function(tester, that /*opt*/) {
        for (var i= 0, n= this.length; i<n; i++)
            if (i in this && tester.call(that, this[i], i, this))
                return true;
        return false;
    };
}

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// IE fallback for CSS visible-control.
(function () {
    if (ie < 9) {

        var applyCssRule = function () {
            // Filter
            var className = $('body').attr('class');
            var group = / *group-([^ ]*)/gi.exec(className);
            var phase = / *phase-([^ ]*)/gi.exec(className);
            if (group && phase) {
                group = group && group[1];
                phase = phase && phase[1];
                var selector_phase = '[data-vc-' + group + '~="' + phase + '"]';
                var selector_all = '[data-vc-' + group + '~="*"]';
                $('.visible-condition').not('.hide').not(selector_phase).not(selector_all).addClass('hide');
                $('.visible-condition.hide').filter(selector_phase + ',' + selector_all).removeClass('hide');
            }
        };

        // Detect for Body className change

        (function (oldcls, callback) {
            document.body.attachEvent('onpropertychange', function (event) {
                if (oldcls != document.body.className) {
                    callback();
                    oldcls = document.body.className;
                }
            });
        })(document.body.className, applyCssRule);

        // Detect for DOM structure change
        // See http://stackoverflow.com/questions/3219758/detect-changes-in-the-dom

        (function (callback, delay) {

            var last = document.getElementsByTagName('*');
            var lastlen = last.length;
            var timer = setTimeout(function check() {

                // get current state of the document
                var current = document.getElementsByTagName('*');
                var len = current.length;

                // if the length is different
                // it's fairly obvious
                if (len != lastlen) {
                    // just make sure the loop finishes early
                    last = [];
                }

                // go check every element in order
                for (var i = 0; i < len; i++) {
                    if (current[i] !== last[i]) {
                        callback();
                        last = current;
                        lastlen = len;
                        break;
                    }
                }

                // over, and over, and over again
                setTimeout(check, delay);

            }, delay);

        }(applyCssRule, 100));

    }
}());
