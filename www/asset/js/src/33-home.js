////////////////////
// Dashboard/Home //
////////////////////

var HomeView = View.extend({

    ui: 'dashboard/home',

    initialize: function () {
        this.once('render', this.showPhase);
        this.listenTo(app.environment, 'change', this.showPhase);
        this.listenTo(app.session, 'change:group', this.showPhase);
    },

    showPhase: function () {
        var group = app.session.get('group');
        var phase = app.environment.get('phase');
        var due = _(app.environment.get('schedule').split(','))
        .map(function (x) { return parseInt(x); });

        this.$('.card').each(function (i, elem) {
            var $card = $(elem);

            // Toggle phase specific display
            var _phase = parseInt($card.data('phase'));
            if (_phase < phase) {
                $card.find('.phase-action').addClass('hide');
            } else if (_phase == phase) {
                $card.find('.phase-action').removeClass('hide');
            } else {
                $card.find('.phase-action').addClass('hide');
            }
            $card.find('[class*="group-"]').addClass('hide');
            $card.find('.group-' + group).removeClass('hide');
            $card.find('[class*="group-non-"]').removeClass('hide');
            $card.find('.group-non-' + group).addClass('hide');

            // Display due time
            var duedate = due[_phase];
            if (duedate) {
                $card.find('.due-date').text(formatDate(new Date(due[_phase])));
            } else {
                $card.find('.due-date').text("待定");
            }

            // Change the style
            $card.removeClass('card-info card-success');
            if (_phase < phase) {
                $card.addClass('card-info');
            } else if (_phase == phase) {
                $card.addClass('card-success');
            }

            $card.removeClass('span4 span8');
            if (_phase == phase) {
                $card.addClass('span8');
            } else {
                $card.addClass('span4');
            }
        });
    },

});

router.register({
    name: 'dashhome',
    dependency: [ 'dashboard' ],
    inlet: function (params, cb) {
        var that = this;

        that.view = new HomeView();
        that.view.once('render', cb);

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;

        // Unload the dashhome view
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    },
});

