///////////////
// Task/Edit //
///////////////


var TaskEditorView = View.extend({

    events: {
        'submit form': 'save',
        'click .task-template-delete': 'deleteTemplate',
        'click .btn-delete': 'showRemoveConfirm',
        'click .btn-cancal-delete, a.close': 'dismissRemoveConfirm',
        'click .btn-confirm-delete': 'deleteItem',
    },

    ui: 'dashboard/task-editor',

    target: function (task) {
        this.$('.date').datepicker();

        if (this.model)
            this.stopListening(this.model);

        this.model = task;

        // Reset the form

        this.$('.btn-group').children().removeClass('active');
        this.$('form')[0].reset();

        if (task) {
            this.$('.btn-delete').removeClass('hide');

            this.listenTo(this.model, 'change', this.target.bind(this, this.model));

            // Fill in existing values

            this.$('input[name="name"]').val(task.get('name'));
            var type = this.model.get("type");
            this.$('.radio-task-type button[data-val="' + type + '"]').addClass('active');
            this.$('.date input[name="due_date"]').val(formatDate(new Date(task.get('due_date') || 0)));
            this.$('input[name="share"]').val(task.get('share'));
            var asset = task.get('asset') || {};
            if (asset.template) {
                this.$('a.btn.task-template-download') .disabled(false)
                .attr('href', asset.template);
                this.$('a.btn.task-template-delete').disabled(false);
            } else {
                this.$('a.btn.task-template-download').disabled(true).removeAttr('href');
                this.$('a.btn.task-template-delete').disabled(true);
            }
        } else {
            this.$('.btn-delete').addClass('hide');
            this.$('a.btn.task-template-download').disabled(true).removeAttr('href');
            this.$('a.btn.task-template-delete').disabled(true);
            this.$('.radio-task-type button[data-val="upload"]').addClass('active');
        }
        return false;
    },

    deleteTemplate: function() {
        var that = this;
        if (this.$('.task-template-delete').disabled()) return;
        console.log('deleting resume');

        var task = {
            asset: {
                template: '',
            }
        };

        $.post(that.model.url(), task, function(data) {
            if (data.err) {
                app.notification
                .error("<b>删除失败</b> 文档模版")
                .show(2000);

            } else {
                app.notification
                .info("<b>已删除</b> 文档模版")
                .show(2000);

                $.del(that.model.get("asset").template, noop());
                that.model.set(data);
            }
        })
    },

    uploadTemplate: function (cb) {
        var that = this;
        var uploadingnote = app.notification
        .info("<b>正在上传</b> 文档模版")
        .show(10000);

        $.upload(this.$(':file[name="template"]'), function (data) {
            uploadingnote.dismiss();
            that.$('.fileupload').fileupload('clear').removeData('fileupload').fileupload();
            if (data.err) {
                app.notification
                .error("<b>上传失败</b> 文档模版")
                .show(5000);
                cb(false, null);
            } else {
                app.notification
                .success("<b>上传成功</b> 文档模版")
                .show(2000)
                cb(true, data);
            }
        });
    },

    postTask: function(_task) {

        var task = this.model;

        _(_task).extend({
            name: this.$('input[name="name"]').val(),
            type: this.$('.radio-task-type button.active').data('val'),
            due_date: Date.parse(this.$('.date input[name="due_date"]').val()),
            share: this.$('input[name="share"]').val()
        });

        if (task) {

            var patch = _.patch(task.attributes, _task);
            if (_.size(patch)) {
                $.post(this.model.url(), patch, function (data) {
                    if (data.err) {
                        app.notification
                        .error("<b>失败</b> 无法更新任务信息")
                        .show(5000);

                    } else {
                        app.notification
                        .success("<b>成功</b> 已更新任务信息")
                        .show(2000);

                        task.set(patch);
                    }
                });
            }

        } else {

            $.post('/task/', _task, function (data) {
                if (data.err) {
                    app.notification
                    .error("<b>失败</b> 无法创建任务信息")
                    .show(5000);
                } else {
                    app.notification
                    .success("<b>成功</b> 已创建任务信息")
                    .show(2000);

                    app.task.fetch({
                        success: function() {
                            router.navigate(
                                'dashboard/task/' + data.id + '/edit',
                                { trigger: true, replace: true}
                            );
                        }
                    })
                }
            });

        }

    },

    save: function () {
        var that = this;
        var task = that.model;

        if ($(':file[name="template"]').val()) {
            that.uploadTemplate(function(result, data) {
                if (result) {
                    that.postTask({
                        asset: data
                    });
                }
            });
        } else {
            that.postTask({});
        }

        return false;
    },

    showRemoveConfirm: function () {
        this.$('.note-delete').removeClass('hide');
    },

    dismissRemoveConfirm: function () {
        this.$('.note-delete').addClass('hide');
    },

    deleteItem: function () {
        var task = this.model;

        if (task) {
            $.del(task.url(), function (data) {
                if (data.err) {
                    app.notification
                    .error("<b>失败</b> 无法删除该任务")
                    .show(3000);
                } else {
                    app.notification
                    .success("<b>已删除</b> 任务 " + task.get('name'))
                    .show(3000);
                    app.task.fetch({
                        success: function () {
                            router.navigate(
                                'dashboard/task',
                                { trigger: true, replace: true }
                            );
                        }
                    });
                }
            });
        }

        this.dismissRemoveConfirm();
    },
});

router.register({
    name: 'dashtask-edit',
    dependency: [ 'dashtask' ],
    inlet: function (params, cb) {
        var that = this;

        that.view = new TaskEditorView();
        that.view.once('render', function() {
            that.shift([ params.shift() ], cb);
        });

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
        that.find('dashtask').view.$el.hide(); // Hide task view
    },
    outlet: function (params, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        that.find('dashtask').view.$el.show(); // Show task view
        return cb();
    },
    shift: function (param, cb) {
        var that = this;
        var id = param.shift();

        if (id == '+') {
            // Create new item
            that.view.target();
        } else {
            // Edit existing item
            that.view.target(app.task.get(id));
        }
        return cb();
    }
})
