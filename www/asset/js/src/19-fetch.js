///////////////////
// Fetch Service //
///////////////////

var fetch = app.fetch = {};

// Fetch Profile
fetch.profile = function (uid, container) {
    var obj = cache('/u/' + uid + '/profile', function (data) {
        obj.id = uid;
        obj.set(data);
    });
    if (obj.__cache__ == 'pending') {
        obj = container || new ProfileModel();
    } else if (container) {
        container.set(obj.attributes);
        obj = container;
    }
    return obj;
};

