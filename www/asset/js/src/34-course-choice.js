////////////////////////////
// Dashboard/CourseChoice //
////////////////////////////

var CourseChoiceView = View.extend({

    events: {
        'click tr': 'open',
        'click .cancel': 'cancel',
    },

    ui: 'dashboard/course-choice',

    initialize: function () {
        this.once('render', this.fetch, this);
        this.listenTo(app.course, 'sync', this.update);
    },

    fetch: function () {
        var that = this;

        $.get('/u/' + app.session.get('uid') + '/choice', function (data) {
            if (data.err) {
                // Data load Error
            } else {
                if (Array.isArray(data)) {
                    var choice = [];

                    data.forEach(function (c) {
                        if (c) choice.push(c);
                        else choice.push(null);
                    });

                    if (choice.length < 3) {
                        choice = choice.concat([null, null, null]).slice(0, 3);
                    }

                    that.choice = choice;

                    that.update();

                } else {
                    // Invalid response
                }
            }
        });
    },

    update: function () {
        var that = this;
        that.$('.choice').each(function (index, elem) {
            var id = that.choice[index] || "";
            var model = id && app.course.get(id);
            var name = model && model.get('enterprise').name +
            " - " + model.get('name') || "";
            $(elem).text(name);
        });
    },

    open: function (ev) {
        var $target = $(ev.currentTarget);
        var id = this.choice[$target.index()];
        if (id) {
            router.navigate('dashboard/course/' + id, { trigger: true });
        } else {
            app.notification
            .info("还未填报该志愿")
            .show(2000);
        }
    },

    cancel: function (ev) {
        var $target = $(ev.currentTarget).parent().parent();
        if (this.choice[$target.index()]) {
            this.choice[$target.index()] = null;
            this.save();
        } else {
            app.notification
            .info("还未填报该志愿")
            .show(2000);
        }
        return false;
    },

    save: function () {
        var that = this;
        $.post('/u/' + app.session.get('uid') + '/choice', {
            choice: that.choice,
        }, function (data) {
            if (data.err) {
                app.notification
                .error("<b>错误</b> 志愿信息保存失败")
                .show(5000);
            } else {
                app.notification
                .success("<b>已保存</b> 志愿信息")
                .show(3000);
            }
            that.fetch();
        });
    },

});

router.register({
    name: 'dashcourse-choice',
    dependency: [ 'dashboard' ],
    inlet: function (params, cb) {

        // Show for Student only
        if (app.session.get('group') != 'student') return cb();
        if (app.environment.get('phase') <= 1) return cb();

        var that = this;

        that.view = new CourseChoiceView();

        that.view.once('render', cb);

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);

    },
    outlet: function (params, cb) {
        var that = this;

        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    },
});

