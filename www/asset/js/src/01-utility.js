/////////////
// Utility //
/////////////

var util = window.util = {};

// Define an no-operation function
var noop = function () {
    var that = this;
    var cb = arguments[arguments.length - 1];
    if (cb && cb instanceof Function) {
        return cb.apply(that);
    }
    return function () { };
};

// Simple render implementation for Backbone.View
var plainRender = function () {
    var data = this.model && this.model.attributes || {};
    this.$el.html(this.template(data));
    this.trigger('render');
    return this;
};

// Import the md5 utility function
var md5 = window.MD5;

// Bind all function in an object's `this' to the that
var bindthat = function (object, that) {
    that = that || object;
    for (var key in object) {
        if (object[key] instanceof Function) {
            object[key].bind(that);
        }
    }
    return object;
};

var randomString = function (len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
};

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

Array.prototype.removeItem = function (item) {
    var idx = this.indexOf(item);
    if (idx != -1) this.remove(idx);
    return this;
};

Array.prototype.pushNX = function (item) {
    if (!~this.indexOf(item)) {
        this.push(item);
        return true;
    } else {
        return false;
    }
};

var setImmediate = (function () {

    // IE10 setImmediate cannot be assigned to any variable, to work around
    // this, bind setImmediate with `null'
    var _setImmediate = window.setImmediate && window.setImmediate.bind(null) || function (cb) {
        return setTimeout(cb, 1);
    };

    // wrapper to call setImediate
    return function (cb) {
        _setImmediate(cb);
    };

})();

_.mixin({
   urlhash: function (url) {
       if ('string' != typeof url) return null;
       var p = url.indexOf('#') + 1;
       if (p == 0) return '';
       return url.substr(p);
   },
   patch: function (a, b) {
       var _a = _(a);
       var _b = _(b);
       var picked = _a.chain()
       .keys().union(_b.keys()).filter(function (key) {
           return a[key] != b[key];
       })
       .value();
       return _b.pick(picked);
   },
});

$.fn.extend({
    disabled: function (val) {
        if (val === true)
            this.attr('disabled', 'disabled');
        else if (val === false)
            this.removeAttr('disabled');
        else
            return this.attr('disabled') == 'disabled';
        return this;
    },
    switchClass: function (prefix, val) {
        this.removeClass(function (index, css) {
            var regex = new RegExp("\\b" + prefix + "\\S+", 'ig');
            return (css.match(regex) || []).join(' ');
        });
        this.addClass(prefix + val);
    },
});

var formatDate = util.formatDate = (function () {
    var format = $.fn.datepicker.DPGlobal.parseFormat('yyyy-mm-dd');
    return function (date) {
        return $.fn.datepicker.DPGlobal.formatDate(date, format, 'zh-CN');
    };
})();

// Deep Comparation of Object
// See: http://stackoverflow.com/a/6713782/1059279
Object.equals = function( x, y ) {
  if ( x === y ) return true;
    // if both x and y are null or undefined and exactly the same

  if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;
    // if they are not strictly equal, they both need to be Objects

  if ( x.constructor !== y.constructor ) return false;
    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.

  for ( var p in x ) {
    if ( ! x.hasOwnProperty( p ) ) continue;
      // other properties were tested using x.constructor === y.constructor

    if ( ! y.hasOwnProperty( p ) ) return false;
      // allows to compare x[ p ] and y[ p ] when set to undefined

    if ( x[ p ] === y[ p ] ) continue;
      // if they have the same strict value or identity then they are equal

    if ( typeof( x[ p ] ) !== "object" ) return false;
      // Numbers, Strings, Functions, Booleans must be strictly equal

    if ( ! Object.equals( x[ p ],  y[ p ] ) ) return false;
      // Objects and Arrays must be tested recursively
  }

  for ( p in y ) {
    if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
      // allows x[ p ] to be set to undefined
  }
  return true;
};

// Figure out type of `obj'
var typeid = util.typeid = function (obj) {
    return Object.prototype.toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};

var signatureof = function (func) {
    var s = func.toString();
    var m = s.match(/^\s*function\s*([^(]*)\s*\(([^)]*)\)\s*\{/i);
    if (m) {
        return {
            name: m[1] || null,
            param: m[2] && m[2].split(/\s*,\s*/) || []
        };
    } else {
        return null;
    }
};

var BBExtend = function (BBObject, ext) {

    var constr = ext.initialize || ext.constructor || noop();

    delete ext.initialize;
    delete ext.constructor;

    var Clazz = function () {

        var args = Array.prototype.slice.call(arguments, 0);

        // Call super constructor
        BBObject.apply(this, args);

        // Call extended constructor
        constr.apply(this, args);

    };

    _.extend(Clazz.prototype, BBObject.prototype, ext);

    Clazz.extend = BBObject.extend;

    return Clazz;

};

