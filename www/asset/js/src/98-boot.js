/////////////////////////////
// Application Entry Point //
/////////////////////////////

$(function () {

    app.$rootview = $('#content');

    // Display Browser Compatibility Alert View

    if (ie < 8) {
        var compatview = new CompatibilityAlertView();
        compatview.render().$el.appendTo('body');
    } else {
        app.session.fetch();
    }

    // Add IE note on html

    if (ie < 9) {
        $('html').addClass('ie' + ie);
    }

    FastClick.attach(document.body); // Attach FastClick

});

Backbone.history.start();

