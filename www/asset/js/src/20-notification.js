///////////////////////
// Notification View //
///////////////////////

var NotificationView = View.extend({

    className: 'wrapper',

    events: {
        'click .close': 'dismiss'
    },

    ui: 'notification',

    initialize: function () {
        this.model = new Model();

        this.listenTo(this.model, 'change', this.render);
    },

    render: function () {
        this.$el.html(this.template());
        this.$('.alert').addClass('alert-' + this.model.get('type'));
        this.$('.text').html(this.model.get('text'));
        this.trigger('render');
        return this;
    },

    dismiss: function () {
        this.$el.removeClass('active');
        setTimeout(function () {
            this.remove();
            this.unbind();
        }.bind(this), 1000);
    },

    show: function (duration) {
        this.$el.appendTo('#notifications');
        setTimeout(function () {
            this.$el.addClass('active');
        }.bind(this), 1);
        if (duration) {
            setTimeout(this.dismiss.bind(this), duration);
        }
        return this;
    }

});

// Get the tempalte resources first, so when we want to notify our user, we
// don't need to wait the process of template resource downloading completed.
uirequest('notification');

app.notification = function (type, text) {
    var note = new NotificationView();
    note.model.set({
        text: text,
        type: type
    });
    return note;
};

_(app.notification).extend({
    success: function (text) {
        return app.notification('success', text);
    },
    error: function (text) {
        return app.notification('error', text);
    },
    info: function (text) {
        return app.notification('info', text);
    },
    warn: function (text) {
        return app.notification('warning', text);
    }
});

