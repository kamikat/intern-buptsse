/////////////////////////////
// Administration/Schedule //
/////////////////////////////

var AdmScheduleEditView = View.extend({

    events: {
        'submit .form-schedule': 'save',
        'click .form-phase .btn-prev': 'prevPhase',
        'click .form-phase .btn-next': 'nextPhase',
    },

    ui: 'administration/schedule',

    initialize: function () {
        this.once('render', this.show);
        this.listenTo(app.environment, 'change:schedule', this.show);
        this.listenTo(app.environment, 'change:phase', this.show);
    },

    show: function () {
        this.$('.date').datepicker();

        var due = _(app.environment.get('schedule').split(','))
        .map(function (x) { return parseInt(x); });
        this.$('.form-schedule .date').each(function (i, elem) {
            var $dp = $(elem);
            if (due[i]) {
                $dp.find('input').val(formatDate(new Date(due[i])));
            } else {
                $dp.find('input').val('');
            }
        });

        var _phase = parseInt(app.environment.get('phase'));

        this.$('.form-phase input[name="phase"]').val(_phase + "-" + SA_PHASENAME[_phase]);

        if (SA_PHASENAME[_phase - 1]) {
            this.$('.form-phase button.btn-prev')
            .removeClass('hide')
            .text("退回 " + SA_PHASENAME[_phase - 1]);
        } else {
            this.$('.form-phase button.btn-prev')
            .addClass('hide');
        }

        if (SA_PHASENAME[_phase + 1]) {
            this.$('.form-phase button.btn-next')
            .removeClass('hide')
            .text("进入 " + SA_PHASENAME[_phase + 1]);
        } else {
            this.$('.form-phase button.btn-next')
            .addClass('hide');
        }

        this.$('.form-phase input[name="password"]').val('');
    },

    save: function () {
        var due = $.map(
            this.$('.form-schedule .date input').toArray(),
            function (elem, i) {
                return Date.parse($(elem).val()) || 0;
            }
        ).join(',');

        if (due != app.environment.get('schedule')) {
            $.post('/environment', {
                schedule: due
            }, function (data) {
                if (data.err) {
                    // TODO Error Occurred
                } else {
                    app.notification
                    .success('<b>已更新</b> 计划设定')
                    .show(2000);
                }
                app.environment.fetch();
            });
        }

        return false;
    },

    chPhase: function (phase) {
        $.post('/environment', {
            phase: phase,
            password: md5(this.$('input[name="password"]').val())
        }, function (data) {
            if (data.err) {
                app.notification
                .error("<b>失败</b> 无法切换系统阶段, 请查证您输入的密码")
                .show(5000);
            } else {
                app.notification
                .success("<b>已更新</b> 已经切换到 " +
                         SA_PHASENAME[phase] + " 阶段")
                .show(5000);
            }
            app.environment.fetch();
        });
    },

    prevPhase: function () {
        this.chPhase(parseInt(app.environment.get('phase')) - 1);
    },

    nextPhase: function () {
        this.chPhase(parseInt(app.environment.get('phase')) + 1);
    },

});

router.register({
    name: 'admschedule',
    dependency: [ 'dashadmin' ],
    inlet: function (param, cb) {
        var that = this;

        that.view = new AdmScheduleEditView();
        that.view.once('render', cb);

        var $viewport = that.find('dashadmin').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (param, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        return cb();
    },
});

