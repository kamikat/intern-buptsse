
var cd = app.cd = _({

    ///
    // Component Registration
    //
    component: function (name) {
        return cd.component[name];
    },

    ///
    // Component Meet Filters
    //
    //      function () {
    //          _.extend(this, {
    //              ...
    //          });
    //      };
    //
    __filter__: [ ],

    filter: function (filter) {
        this.__filter__.push(filter);
    },

    ///
    // Register Component (Use `meet' because we need to say `hi' and `bye' to components)
    //
    meet: function (component) {

        var that = this;

        // Missing component name
        if (!component.name) {
            console.warn("[Register] Missing component name.");
        }

        _.defaults(component, {

            // Dependency can be either static dependency array or dynamic dependency function
            dependency: [],

            // Called to setup a component
            inlet: noop(),

            // Called to destroy a component
            outlet: noop(),

            ///
            // inlet/outlet function are defined as:
            //
            //      function (require, callback) {
            //                ^        ^ Callback function called to continue the
            //                ^          loading of components.
            //                ^ A require function for search other component
            //                  declared directly or indirectly.
            //      }
            ///

        });

        // Accept simple string as value of `component.dependency'
        if ('string' == typeid(component.dependency)) {
            component.dependency = [ component.dependency ];
        }

        // Transform dependency array to dynamic dependency function
        if ('array' == typeid(component.dependency)) {

            // Filter invalid dependency values
            component.dependency = _.compact(component.dependency);

            var dependency_ = component.dependency;
            component.dependency = function (dependency) {
                return _.filter(dependency_, function (item) {
                    return !~dependency.indexOf(item);
                });
            };

        }

        // Expose Component Library to Component
        var require = component.require = component.component = that.component;

        // Apply filters to component
        _(that.__filter__).each(function (filter) {
            filter.call(component);
        });

        ///
        // Control Logic
        //  Responsible to manage state of components
        ///

        var diag = that;

        // Component Load/Unload Automaton
        _.extend(component, {

            // Default state is `invalid'
            state: 'invalid',

            ///
            // State Machine Definition:
            //
            //  (this.state == <current state>) && (this.state = <next state>, <actions>);
            //
            ///

            __inject__: function () {
                (this.state == 'invalid'    ) && (this.state = 'injecting',
                                                  this.inlet(require, this.__injected__.bind(this)));
                (this.state == 'ejecting'   ) && (this.state = 'toinject' );
                (this.state == 'toeject'    ) && (this.state = 'injecting');
            },

            __injected__: function () {
                (this.state == 'injecting'  ) && (this.state = 'ready'    ,
                                                  diag.trigger('injected:' + this.name), diag.trigger('injected', this.name));
                (this.state == 'toeject'    ) && (this.state = 'ejecting' ,
                                                  this.outlet(require, this.__ejected__.bind(this)));
            },

            __eject__: function () {
                (this.state == 'ready'      ) && (this.state = 'ejecting' ,
                                                  this.outlet(require, this.__ejected__.bind(this)));
                (this.state == 'injecting'  ) && (this.state = 'toeject'  );
                (this.state == 'toinject'   ) && (this.state = 'ejecting' );
            },

            __ejected__: function () {
                (this.state == 'ejecting'   ) && (this.state = 'invalid'  ,
                                                  diag.trigger('ejected:' + this.name), diag.trigger('ejected', this.name));
                (this.state == 'toinject'   ) && (this.state = 'injecting',
                                                  this.inlet(require, this.__injected__.bind(this)));
            },

        });

        // Component Dependency Solution
        _.extend(component, {

            // Components depend on this component
            __ref__: [ ],

            // Components depended by this component
            __dependency__: [ ],

            // Pending dependency loading tasks
            //  Consume by __check__ function everytime new module are loaded
            __pending__: [ ],

            // Get hi from friend
            __hi__: function (who) {
                if (!~this.__ref__.indexOf(who)) {
                    this.__ref__.push(who);
                }
                return this.__step__();
            },

            // A flag to detemine wether the dependency is resolving, all
            // further __step__ operation will be pending until the current
            // dependency resolve task is completed.
            __dependency_resolving__: false,

            __step__: function (dependency) {
                var that = this;

                if ('undefined' == typeid(dependency)) {
                    if (!this.__dependency_resolving__) {
                        // Start an async dependency resolve task
                        this.__dependency_resolving__ = true;
                        return this.dependency(this.__dependency__, function (result) {
                            // Use `null' to differ from undefined
                            return that.__step__(result || null);
                        });
                    } else {
                        // Waiting for existing resolve task
                        return;
                    }
                }

                // Set dependency resolving state to false
                this.__dependency_resolving__ = false;

                // Break if this should not be loaded
                if (!this.__ref__.length) return;

                if ('null' == typeid(dependency)) {
                    dependency = [ ];
                }

                if ('string' == typeid(dependency)) {
                    dependency = [ dependency ];
                }

                if ('array' != typeid(dependency)) {
                    throw new TypeError("Expect String or Array result from dependency generator.");
                }

                if (!dependency.length) {
                    // Time that all dependency ready
                    return this.__inject__();
                }

                _(dependency).each(function (v) {
                    this.__dependency__.pushNX(v);

                    // If not ready, send to __pending__ for __check__
                    if (require[v].state != 'ready') {
                        this.__pending__.pushNX(v);
                    }

                    diag.trigger('hi:' + v, this.name);
                }, this);

                if (!this.__pending__.length) {
                    return this.__step__();
                }
            },

            // Check when any friend is avaliable,
            // if all available, find more friend (call __step__).
            __check__: function (name) {
                if (!~this.__pending__.indexOf(name)) return;

                this.__pending__.removeItem(name);

                if (!this.__pending__.length) {
                    return this.__step__();
                }
            },

            // Get bye from friend
            __bye__: function (who) {
                this.__ref__.removeItem(who);

                if (!this.__ref__.length) {
                    if (this.state == 'invalid') {
                        // Cancelled when loading dependency
                        return this.__post_bye__();
                    } else {
                        return this.__eject__();
                    }
                }
            },

            // Say bye to friends
            __post_bye__: function () {
                while (this.__dependency__.length) {
                    diag.trigger('bye:' + this.__dependency__.pop(), this.name);
                }

                // Clear pending load task array
                this.__pending__.length = 0;
            },

        });

        that.on('hi:'      + component.name, component.__hi__       , component);
        that.on('injected'                 , component.__check__    , component);
        that.on('bye:'     + component.name, component.__bye__      , component);
        that.on('ejected:' + component.name, component.__post_bye__ , component);

        // Bind `this` to any function in Component
        component = bindthat(component);

        // Generate `__internal__' variable list
        component.__internals__ = [ ];
        component.__internals__ = _.keys(component);

        // Register Component
        that.component[component.name] = component;

    },

    ///
    // Root Nodes
    //
    root: { },

    ///
    // Add Node to Root
    //  One can define a name called `rootname' pointing to a specific
    //  component (that is, `alias` to the component).
    //
    attach: function (rootname, name) {
        var that = this;

        name = name || rootname;

        if (that.root[rootname] != name) {
            that.detach(rootname);
            that.root[rootname] = name;
            that.trigger('hi:' + name, 'root');
        }
    },

    ///
    // Remove Node from Root
    //
    detach: function (rootname) {
        if (rootname) {
            this.trigger('bye:' + this.root[rootname], 'root');
            delete this.root[rootname];
        }
    },

}).extend(Backbone.Events);

///
// Filter: support synchronous definition of inlet/outlet/dependency
// Depend: `inlet', `outlet', `dependency'
// Yield : `inlet', `outlet', `dependency'
//
cd.filter(function () {
    var asynch2c = function (func) {
        // Make function asynchronous when function has less than 2 parameter
        if (func && func.length < 2) {
            // Wrap for async function
            var sync = func;
            return function (arg1, cb) {
                return cb(sync.call(this, arg1) || null);
            };
        } else {
            return func;
        }
    };

    this.dependency = asynch2c(this.dependency);
    this.inlet = asynch2c(this.inlet);
    this.outlet = asynch2c(this.outlet);
});

///
// Filter: `type' resolver
// Depend: `name'
// Yield : `type', `name'
//
cd.filter(function () {
    if ('string' == typeid(this.type)) {
        this.type = [ this.type ];
    }
    var t = this.name.match(/^([^.]+)(\.(.*))?$/i)
    var name = t[1];
    var type = t[3] && t[3].split('.') || [];
    var __type__ = (this.type || []).concat(type);
    this.type = function () {
        var args = Array.prototype.slice.call(arguments, 0);
        return _(args).any(function (cond) {
            if ('string' == typeid(cond)) {
                cond = [ cond ];
            }
            return _(cond).all(function (type) {
                return !!~__type__.indexOf(type);
            });
        });
    };
    this.name = name;
});

///
// Filter: `Event` based component, also apply to `data' component
// Depend: `type'
// Yield : Eventialize
//
cd.filter(function () {
    if (this.type('event', 'data')) {
        _.extend(this, Backbone.Events);

        this.outlet = _.wrap(this.outlet, function (outlet, r, cb) {
            return outlet.call(this, r, function () {
                cb.apply(this, arguments);

                // Automatically stop listening after outlet
                this.stopListening();
            })
        });
    }
});

///
// Filter: static component
// Depend: `type'
// Yield : Attach component to root
//
cd.filter(function () {
    if (this.type('static')) {
        setImmediate(cd.attach.bind(cd, this.name));
    }
});

cd.filter(function () {
    if (this.type('data')) {
        _(this).extend({
            set: function (object) {
                var that = this;
                var changed = false;
                _(object).each(function (v, k) {
                    // DEBUG Helper
                    if (~that.__internals__.indexOf(k)) {
                        console.warn("Variable " + k + " conflicts with internal fields, ignored.");
                        return;
                    }
                    if (that[k] != v) {
                        that[k] = v;
                        that.trigger('change:' + k, that, v);
                        changed = true;
                    }
                });
                if (changed) {
                    that.trigger('change', that);
                }
            },
        });
    }
});

///
// Filter: logger
// Depend: `inlet', `outlet', `dependency'
// Yield : inlet/outlet/dependency log
//
cd.filter(function () {

    var asynchook2c = function (object, member, before, after, ctx) {
        return _.wrap(member, function (func, arg1, cb) {
            before.call(ctx || this);
            func.call(object, arg1, function () {
                after.call(ctx || this);
                cb.apply(this, arguments);
            });
        });
    };

    this.inlet = asynchook2c(this, this.inlet, function () {
        console.log("(cd)  I  " + this.name);
    }, function () {
        console.log("(cd) [I] " + this.name);
    }, this);

    this.outlet = asynchook2c(this, this.outlet, function () {
        console.log("(cd)  E  " + this.name);
    }, function () {
        console.log("(cd) [E] " + this.name);
    }, this);

    this.dependency = asynchook2c(this, this.dependency, function () {
        console.log("(cd)  D  " + this.name);
    }, function () {
        console.log("(cd) [D] " + this.name);
    }, this);

});

