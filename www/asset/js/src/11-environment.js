/////////////////
// Environment //
/////////////////

var SA_PHASENAME = [
    "系统准备",
    "职位发布",
    "实习申报",
    "导师录用",
    "课程调剂",
    "实习进行",
    "导师评分",
    "课程结束"
];

var EnvironmentModel = Model.extend({

    defaults: {
        phase: -1,
        bulletin: '',
        schedule: '',
    }

});

app.environment = new EnvironmentModel();

cd.meet({

    name: 'env.data',

    inlet: function (require, cb) {
        this.check(cb);
    },

    check: function (cb) {
        var that = this;
        cb = cb || noop();
        $.get('/environment', function (env) {
            that.set(env);
            app.environment.set(env);
            return cb(env);
        });
    },

});

cd.meet({

    name: 'phase-selector.static',

    dependency: 'env',

    type: 'event',

    inlet: function (require) {
        this.listenTo(require.env, 'change:phase', this.selector);
        this.selector();
    },

    selector: function () {
        var env = this.require('env');

        $('body').switchClass('phase-', env.phase);
    },

});

