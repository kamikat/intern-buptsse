///////////
// Cache //
///////////

var cache = (function () {

    var _cache = {};

    var func = function (uri, callback) {

        var that = this;
        var cb = callback || noop;

        var cache = _cache[uri];

        if (!cache) {
            var cache = function (obj) {
                cache.__cache__ = 'ready';
                _cache[uri] = obj;
                cache.callback.forEach(function (cb) {
                    // Callbacks are called sequentially
                    return cb.call(that, _cache[uri], undefined);
                });
            }
            cache.__cache__ = 'pending';
            cache.callback = [ cb ];
            $.get(uri, cache);
        } else if (cache.__cache__ == 'pending') {
            cache.callback.push(cb);
        } else {
            setImmediate(cb.bind(that, cache));
        }

        return cache;

    };

    func.cache = _cache;

    func.invalidate = function (uri) {
        var cache = _cache[uri];

        if (cache.__cache__ != 'pending') {
            delete _cache[uri];
        }
    };

    return func;

}());

