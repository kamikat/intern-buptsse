//////////////////////////////
// Dashboard/Administration //
//////////////////////////////

var AdministrationView = View.extend({

    ui: 'dashboard/administration',

    initialize: function () {
        this.listenTo(router, 'route', this.showTab);
    },

    render: function () {
        this.$el.html(this.template());
        this.showTab();
        this.trigger('render');
        return this;
    },

    showTab: function () {
        this.$('.nav li').removeClass('active');
        var selector = _.map(router.subroute(), function (x) {
            return '.nav li a[href="#' + x + '"]';
        });
        this.$(selector.join(', ')).parent().addClass('active');
    },

});

router.register({
    name: 'dashadmin',
    dependency: [ 'dashboard' ],
    inlet: function (params, cb) {
        var that = this;

        that.view = new AdministrationView();
        that.view.once('render', function () {
            that.$viewport = that.view.$('#administration-content');
            return cb();
        });

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);

        this.shift(params, noop);
    },
    outlet: function (params, cb) {
        var that = this;

        // Unload the dashhome view
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    },
    shift: function (params, cb) {
        if (Backbone.history.fragment == 'dashboard/administration') {
            router.navigate('dashboard/administration/bulletin', { trigger: true, replace: true });
        }
        return cb();
    },
});

