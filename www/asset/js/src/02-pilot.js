////////////////////
// Pilot (Router) //
////////////////////

// Extend Backbone.History.prototype.onloadurl
(function () {
    var _loadurl = Backbone.History.prototype.loadUrl;

    Backbone.History.prototype.loadUrl = function () {
        this.trigger('loadurl', this.fragment);
        _loadurl.apply(this, arguments);
    };
}());

var RouterEx = Backbone.Router.extend({

    routes: { },

    // Get current route
    current: function () {
        return this.translate(Backbone.history.fragment);
    },

    subroute: function () {
        return this._subroute(Backbone.history.fragment);
    },

    _subroute: function (fragment) {
        var routes = [];
        var g = fragment.split('/');
        for (var i = 1; i <= g.length; i++) {
            routes.push(g.slice(0, i).join('/'));
        }
        return routes;
    },

    translate: function (fragment) {
        var that = this;
        var routes = _.pairs(that.routes);

        var route = null;
        var params = null;

        var matched = _.find(routes, function(handler) {
            route = _.isRegExp(handler[0]) ? handler[0] : that._routeToRegExp(handler[0]);
            return route.test(fragment);
        });

        if (matched) {
            // NEW: Extracts the params using the internal
            // function _extractParameters
            params = that._extractParameters(route, fragment);
            route = matched[1];
        } else {
            route = undefined;
        }

        return {
            route : route,
            fragment : fragment,
            params : params
        };
    }

});

var pilot = app.pilot = new RouterEx();

cd.meet({

    name: 'pilot.static',

    type: 'event',

    inlet: function () {
        this.listenTo(Backbone.history, 'loadurl', this.loadurl);
        this.listenTo(this, 'route', this.onroute);
    },

    // Transform and broadcast route message for specified router
    onroute: function (route, params, fragment) {
        return this.trigger('route:' + route, params, fragment);
    },

    // Handle hash change event, load new view as root
    loadurl: function (fragment) {
        var route = pilot.translate(fragment);

        if (route) {
            cd.attach('viewroot', route.route);

            // Trigger route events
            this.trigger('route', route.route, route.params, route.fragment);
        } else {
            cd.detach('viewroot');
        }

        this.fragment = fragment;
    },

    // The last fragment
    fragment: undefined,

    // Expose `navigate' method (options.trigger default to `true'
    navigate: function (fragment, options) {
        return pilot.navigate(fragment, _.defaults(options, {
            trigger: true
        }));
    },

});

///
// Filter: route
// Depend: `type', `hash', `name'
// Yield : (none)
//
// Register route for view component
//
cd.filter(function () {
    if (this.type('view', 'target') && this.hash) {
        pilot.routes[this.hash] = this.name;
    }
});

