////////////////
// Activation //
////////////////

var ActivationView = View.extend({

    events: {
        'submit form': 'active'
    },

    ui: 'activation',

    initialize: function () {
        this.once('render', this.datachange);
        this.listenTo(app.profile, 'change', this.datachange);
    },

    datachange: function () {
        this.$('.input-name input').val(app.profile.get("name"));
        this.$('.input-phone input').val(app.profile.get("phone"));
        this.$('.input-email input').val(app.profile.get("email"));
    },

    active: function () {
        var that = this;

        var profile = {};

        if (that.notification) {
            (function (notification) {
                setTimeout(function () {
                    notification.dismiss();
                }, 200);
            })(that.notification);
            that.notification = undefined;
        }

        profile.name = that.$('.input-name input').val();
        profile.phone = that.$('.input-phone input').val();
        profile.email = that.$('.input-email input').val();

        $.post(app.profile.url(), profile, function (data) {
            if (data.err || !data.active) {
                that.notification = app.notification
                .error("<b>激活失败</b> 请再次确认您填写的信息真实有效")
                .show(2000);
            } else {
                app.notification
                .success("<b>账户激活</b> 欢迎您, " + profile.name)
                .show(2000);

                app.profile.set(data);
            }
        });
        return false;
    }

});

router.register({
    name: 'activation',
    inlet: function (params, cb) {
        var that = this;

        that.view = new ActivationView();
        that.view.once('render', cb);
        that.view.render().$el.appendTo(app.$rootview);

        // Check if activation is needed
        app.profile.trigger('change');
    },
    outlet: function (params, cb) {
        var that = this;

        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    }
});

