//////////////////////
// Dashboard/Course //
//////////////////////

var CourseView = View.extend({

    events: { },

    ui: 'dashboard/course',

    initialize: function () {

        // XXX Not Efficient

        var that = this;

        that.listenTo(that.collection, 'request', function requestHandler () {
            that.stopListening(that.collection);
            that.listenTo(that.collection, 'sync', function () {
                that.render();
                that.listenTo(that.collection, 'add', that.render);
                that.listenTo(that.collection, 'change', that.render);
                that.listenTo(that.collection, 'remove', that.render);
                that.listenTo(that.collection, 'reset', that.render);
            });
            that.listenTo(that.collection, 'request', requestHandler);
        });
    },

    render: function () {
        var that = this;

        that.$el.html(that.template());

        that.collection.each(function (course) {
            var view = new CourseItemView({
                model: course
            });
            that.$('tbody').append(view.render().el);
        });

        that.trigger('render');

        return that;
    },

});

var CourseItemView = View.extend({

    tagName: 'tr',

    events: {
        'click': 'edit'
    },

    ui: 'dashboard/course-item',

    edit: function () {
        if (~['professor', 'admin'].indexOf(app.session.get('group')) && app.session.get('phase') < 2) {
            router.navigate(
                'dashboard/course/' + this.model.id + '/edit',
                { trigger: true }
            );
        } else {
            router.navigate(
                'dashboard/course/' + this.model.id,
                { trigger: true }
            );
        }
    },

});

router.register({
    name: 'dashcourse',
    dependency: [ 'dashboard', 'dashcourse-choice' ],
    inlet: function (params, cb) {
        var that = this;

        that.view = new CourseView({
            collection: app.course
        });

        app.course.fetch();

        that.view.once('render', cb);

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;

        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    },
});

