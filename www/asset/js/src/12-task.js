////////////////
// Task Model //
////////////////

var TaskModel = Model.extend({

    defaults: {
        name: '',
        type: '',
        asset: { },
        share: 0.0,
        due_date: 0,
        creator: '',
        created_at: 0
    },

});

var TaskCollection = Collection.extend({

    url: '/task/',

    model: TaskModel,

});

app.task = new TaskCollection();

