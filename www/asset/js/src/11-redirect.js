//////////////
// Redirect //
//////////////

cd.meet({

    name: 'redirect.static',

    dependency: [
        'session_redirect',
    ],

});

///
// Redirect based on session condition
//
cd.meet({

    name: 'session_redirect',

    dependency: [ 'session', 'pilot' ],

    type: 'event',

    inlet: function (require, cb) {
        this.listenTo(require.session, 'change:authenticated', this.check);
        this.listenTo(require.pilot, 'route', this.route);
        this.route();
        this.check();
    },

    outlet: function (require, cb) {
        this.stopListening();
    },

    blacklist: ['login', 'logout', ''],

    backref: null,

    route: function (route, params, fragment) {
        if (!arguments.length) {
            fragment = this.require('pilot').fragment;
        }
        this.backref = (
            (_(this.blacklist).contains(fragment))
            ? this.backref
            : fragment
        );
    },

    check: function () {
        var session = this.require('session');
        var pilot = this.require('pilot');

        if (session.authenticated) {
            pilot.navigate(this.backref || 'dashboard', { replace: true });
        } else {
            pilot.navigate('login', { replace: true });
        }
    },

});

