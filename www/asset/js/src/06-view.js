////////////////////////////
// Extended Backbone View //
////////////////////////////

var View = BBExtend(Backbone.View, {

    initialize: function () {

        // Load UI

        var uitpl = this.ui;

        if ('function' == typeid(uitpl)) {
            uitpl = uitpl();
        }

        if ('string' == typeid(uitpl)) {
            ui(this, uitpl);
        }

    },

    render: plainRender,

});

