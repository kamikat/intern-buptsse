////////////
// Logout //
////////////

cd.meet({

    name: 'logout.target',

    dependency: 'session',

    hash: 'logout',

    inlet: function (require) {
        var session = require('session');

        session.logout(function (data) {
            app.notification
            .info("<b>注销</b> 会话已经注销")
            .show(3000);
        });
    },

});

