/////////////
// Session //
/////////////

var Session = Model.extend({
    url: '/session'
});

// Session Object:
//  authenticated: true/false
//  username: <string>
//  group: 'student'/'professor'/'admin'

// Singleton session instance
app.session = new Session();

cd.meet({

    name: 'session.data',

    inlet: function (require, cb) {
        this.check(cb);
        this.listenTo(this, 'change:authenticated', this.__authenticate__);
    },

    __authenticate__: function (that, v) {
        app.session.fetch();
        if (v) that.trigger('authenticated');
        else that.trigger('deauthenticated');
    },

    check: function (cb) {
        var that = this;
        cb = cb || noop();
        $.get('/session', function (session) {
            that.set(session);
            return cb(session);
        });
    },

    login: function (username, password, cb) {
        var that = this;
        cb = cb || noop();
        $.post('/session', {
            username: username,
            password: md5(password)
        }, function (session) {
            if (session.err) return cb(session);
            that.set(session);
            return cb(session);
        });
    },

    logout: function (cb) {
        var that = this;
        cb = cb || noop();
        $.del('/session', function (session) {
            document.cookie = '';
            that.set(session);
            return cb(session);
        });
    },

});

cd.meet({

    name: 'group-selector.static',

    dependency: 'session',

    type: 'event',

    inlet: function (require) {
        this.listenTo(require.session, 'change:group', this.selector);
        this.selector();
    },

    selector: function () {
        var session = this.require('session');

        $('body').switchClass('group-', session.group);
    },

});

