/////////////////
// Course/View //
/////////////////

var CourseViewerView = View.extend({

    events: {
        'click .make-choice': 'makeChoice',
        'click .btn-edit': 'edit',
    },

    ui: 'dashboard/course-view',

    target: function (course) {

        var that = this;

        if (this.model)
            this.stopListening(this.model);

        this.model = course;

        if (course) {

            this.listenTo(this.model, 'change', this.target.bind(this, this.model));

            // Fill in existing values

            this.$('.field-name').text(course.get('name'));
            this.$('.field-department').text(course.get('department'));
            this.$('.field-demand').text(course.get('demand'));
            this.$('.field-salary').text({
                lt1000: "<1000",
                ap1500: "1000-2000",
                ap2500: "2000-3000",
                gt3000: ">3000",
            }[course.get('salary')]);
            this.$('.field-brief').text(course.get('brief'));

            var mentor = course.get('mentor');

            this.$('.field-mentor-name').text(mentor.name);
            var mentor_gender = mentor.gender;
            this.$('.field-mentor-gender').text({
                m: "先生",
                f: "女士",
                "": "未知",
            }[mentor_gender || ""]);
            this.$('.field-mentor-position').text(mentor.position);
            this.$('.field-mentor-phone').text(mentor.phone);
            this.$('.field-mentor-email').text(mentor.email);

            var enterprise = course.get('enterprise');

            this.$('.field-enterprise-name').text(enterprise.name);
            this.$('.field-enterprise-website').text(enterprise.website);
            this.$('.field-enterprise-address').text(enterprise.address);

            this.listenTo(app.environment, 'change:phase', function () {
                if (app.environment.get('phase') >= 2) {
                    $.get('/course/' + that.model.id + '/choice', function (data) {
                        if (data.err) {
                            // Error Loading Data
                        } else {
                            _(data).each(function (value, key) {
                                that.$('.selected-as-choice.' + key).html(value.join(" "));
                            });
                        }
                    });
                }
            });
            app.environment.trigger('change:phase');

        } else {

            // Course does not exists

        }

    },

    makeChoice: function (ev) {
        var that = this;
        var choice = that.choice.choice;
        var $target = $(ev.currentTarget).parent();

        if (~choice.indexOf(that.model.id)) {
            choice[choice.indexOf(that.model.id)] = null;
        }

        choice[$target.index()] = that.model.id;
        that.choice.save();

        ev.preventDefault();
    },

    edit: function () {
        router.navigate(
            'dashboard/course/' + this.model.id + '/edit',
            { trigger: true }
        );
    }

});

router.register({
    name: 'dashcourse-view',
    dependency: [ 'dashcourse' ],
    inlet: function (param, cb) {
        var that = this;

        that.view = new CourseViewerView();
        that.view.choice = that.find('dashcourse-choice').view;
        that.view.once('render', function () {
            that.shift([ param.shift() ], cb);
        });

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);

        that.find('dashcourse').view.$el.hide(); // Hide course view
    },
    outlet: function (param, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        that.find('dashcourse').view.$el.show(); // Show course view
        return cb();
    },
    shift: function (param, cb) {
        var that = this;
        var id = param.shift();

        that.view.target(app.course.get(id));

        return cb();
    }
});

