/////////////////
// Course/Edit //
/////////////////

var CourseEditorView = View.extend({

    events: {
        'submit form': 'save',
        'click .btn-delete': 'showRemoveConfirm',
        'click .btn-cancel-delete, a.close': 'dismissRemoveConfirm',
        'click .btn-confirm-delete': 'deleteItem',
    },

    ui: 'dashboard/course-editor',

    target: function (course) {

        if (this.model)
            this.stopListening(this.model);

        this.model = course;

        // Reset the form

        this.$('.btn-group').children().removeClass('active');
        this.$('form')[0].reset();
        this.dismissRemoveConfirm();

        if (course) {

            this.$('.btn-delete').removeClass('hide');

            this.listenTo(this.model, 'change', this.target.bind(this, this.model));

            // Fill in existing values

            this.$('input[name="name"]').val(course.get('name'));
            this.$('input[name="department"]').val(course.get('department'));
            this.$('input[name="demand"]').val(course.get('demand'));
            var salary = course.get('salary');
            this.$('.radio-salary button[data-val="' + salary + '"]')
            .addClass('active');
            this.$('textarea[name="brief"]').val(course.get('brief'));

            var mentor = course.get('mentor');

            this.$('input[name="mentor_name"]').val(mentor.name);
            var mentor_gender = mentor.gender;
            this.$('.radio-mentor-gender button[data-val="' + mentor_gender + '"]')
            .addClass('active');
            this.$('input[name="mentor_position"]').val(mentor.position);
            this.$('input[name="mentor_phone"]').val(mentor.phone);
            this.$('input[name="mentor_email"]').val(mentor.email);

            var enterprise = course.get('enterprise');

            this.$('input[name="enterprise_name"]').val(enterprise.name);
            this.$('input[name="enterprise_website"]').val(enterprise.website);
            this.$('input[name="enterprise_address"]').val(enterprise.address);

        } else {

            this.$('.btn-delete').addClass('hide');

        }

        return false;
    },

    save: function () {
        var that = this;
        var course = that.model;

        var _course = {
            name: this.$('input[name="name"]').val(),
            department: this.$('input[name="department"]').val(),
            salary: that.$('.radio-salary button.active').data('val'),
            demand: parseInt(that.$('input[name="demand"]').val()) || 0,
            brief: this.$('textarea[name="brief"]').val(),
            mentor: {
                name: this.$('input[name="mentor_name"]').val(),
                gender: that.$('.radio-mentor-gender button.active').data('val') || '',
                position: this.$('input[name="mentor_position"]').val(),
                phone: this.$('input[name="mentor_phone"]').val(),
                email: this.$('input[name="mentor_email"]').val(),
            },
            enterprise: {
                name: this.$('input[name="enterprise_name"]').val(),
                website: this.$('input[name="enterprise_website"]').val(),
                address: this.$('input[name="enterprise_address"]').val(),
            },
        }

        if (course) {

            var patch = _.patch(course.attributes, _course);

            var patch_mentor = _.patch(course.attributes.mentor, _course.mentor);
            var patch_enterprise = _.patch(course.attributes.enterprise, _course.enterprise);

            if (_.size(patch_mentor)) {
                // Sub-attribute does not support patch update,
                // so, update in bundle
                patch.mentor = _course.mentor;
            } else {
                delete patch.mentor;
            }

            if (_.size(patch_enterprise)) {
                // Sub-attribute does not support patch update,
                // so, update in bundle
                patch.enterprise = _course.enterprise;
            } else {
                delete patch.enterprise;
            }

            if (_.size(patch)) {
                $.post(that.model.url(), patch, function (data) {
                    if (data.err) {
                        app.notification
                        .error("<b>失败</b> 无法保存新的职位信息")
                        .show(5000);
                    } else {
                        app.notification
                        .success("<b>已更新</b> 职位" + _course.enterprise.name + ' ' + _course.name)
                        .show(3000);
                        course.set(data);
                    }
                });
            }

        } else {

            $.post('/course/', _course, function (data) {
                if (data.err) {
                    app.notification
                    .error("<b>失败</b> 无法创建该职位")
                    .show(5000);
                } else {
                    app.notification
                    .success("<b>已创建</b> 职位 " + _course.enterprise.name + ' ' + _course.name)
                    .show(3000);

                    app.course.fetch({
                        success: function () {
                            router.navigate(
                                'dashboard/course/' + data.id + '/edit',
                                { trigger: true, replace: true }
                            );
                        }
                    });
                }
            });

        }

        return false;
    },

    showRemoveConfirm: function () {
        this.$('.note-delete').removeClass('hide');
    },

    dismissRemoveConfirm: function () {
        this.$('.note-delete').addClass('hide');
    },

    deleteItem: function () {
        var course = this.model;

        if (course) {
            $.del(course.url(), function (data) {
                if (data.err) {
                    app.notification
                    .error("<b>失败</b> 无法删除该职位")
                    .show(3000);
                } else {
                    app.notification
                    .success("<b>已删除</b> 职位 " + course.get('enterprise').name + ' ' + course.get('name'))
                    .show(3000);
                    app.task.fetch({
                        success: function () {
                            router.navigate(
                                'dashboard/course',
                                { trigger: true, replace: true }
                            );
                        }
                    });
                }
            });
        }

        this.dismissRemoveConfirm();
    },

});

router.register({
    name: 'dashcourse-edit',
    dependency: [ 'dashcourse' ],
    inlet: function (param, cb) {
        var that = this;

        that.view = new CourseEditorView();
        that.view.once('render', function () {
            that.shift([ param.shift() ], cb);
        });

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);

        that.find('dashcourse').view.$el.hide(); // Hide course view
    },
    outlet: function (param, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        that.find('dashcourse').view.$el.show(); // Show course view
        return cb();
    },
    shift: function (param, cb) {
        var that = this;
        var id = param.shift();

        if (id == '+') {
            // Create new item
            that.view.target();
        } else {
            // Edit existing item
            that.view.target(app.course.get(id));
        }

        return cb();
    }
});

