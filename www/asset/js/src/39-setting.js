///////////////////////
// Dashboard/Setting //
///////////////////////

var SettingView = View.extend({

    events: {
        'submit #change-profile': 'updateProfile',
        'submit #change-password': 'updatePassword',
        'click .profile-resume-delete': 'deleteResume',
    },

    ui: 'dashboard/setting',

    initialize: function() {
        this.listenTo(app.profile, 'change', this.showProfile);
    },

    render: function () {
        this.$el.html(this.template());
        this.showProfile();
        this.trigger('render');
        return this;
    },

    showProfile: function () {
        var that = this;
        that.$('input[name="name"]').val(app.profile.get('name'));
        that.$('input[name="id"]').val(app.profile.id);
        that.$('input[name="phone"]').val(app.profile.get('phone'));
        that.$('input[name="email"]').val(app.profile.get('email'));
        that.$('.profile-photo').attr('src', app.profile.get('photo'));
        var resume = app.profile.get('resume');
        if (resume) {
            that.$('.profile-resume-download').disabled(false).attr('href', resume);
            that.$('.profile-resume-delete').disabled(false);
        } else {
            that.$('.profile-resume-download').disabled(true).removeAttr('href');
            that.$('.profile-resume-delete').disabled(true);
        }
    },

    updateProfile: function() {
        var that = this;
        var profile = {};

        profile.phone = that.$('input[name="phone"]').val();
        profile.email = that.$('input[name="email"]').val();

        var send = function () {
            var patch = _.patch(app.profile.attributes, profile);
            if (!_.size(patch)) return;
            $.post(app.profile.url(), patch, function(data) {
                if (data.err) {
                    // TODO Display error message
                } else {
                    app.notification
                    .success("<b>已更新</b> 基本资料")
                    .show(2000);

                    app.profile.set(data);
                }
            });
        };

        if (that.$(':file[name="resume"]').val()) {
            // A resume file is selected

            var uploadingnote = app.notification
            .info("<b>正在上传</b> 个人简历")
            .show(10000);

            $.upload(that.$(':file[name="resume"]'), function (data) {
                uploadingnote.dismiss();
                if (data.err) {
                    app.notification
                    .error("<b>上传失败</b> 个人简历")
                    .show(5000);
                } else {
                    app.notification
                    .success("<b>上传成功</b> 个人简历")
                    .show(2000);

                    profile.resume = data.resume;
                }
                that.$('.fileupload').fileupload('clear').removeData('fileupload').fileupload();
                send();
            });
        } else {
            send();
        }

        return false;
    },

    deleteResume: function () {
        if (this.$('.profile-resume-delete').disabled()) return;
        $.post(app.profile.url(), {
            resume: ''
        }, function(data) {
            if (data.err) {
                // TODO Display error message
            } else {
                app.notification
                .info("<b>已删除</b> 个人简历")
                .show(2000);

                $.del(app.profile.get('resume'), noop());

                app.profile.set(data);
            }
        });
    },

    updatePassword: function() {
        var that = this;
        var req = {};

        req.newpass = that.$('input[name="newpass"]').val();
        if (req.newpass != that.$('input[name="confirmpass"]').val()) {
            app.notification
            .error("<b>密码修改失败</b> 两次输入的新密码不一致")
            .show(2000);
            return false;
        }

        req.oldpass = md5(that.$('input[name="oldpass"]').val());
        req.newpass = md5(req.newpass);

        $.post('/passwd', req, function(data) {

            if (data.err) {
                app.notification
                .error("<b>密码修改失败</b> 请再次确认您的原密码")
                .show(2000);
            } else {
                app.notification
                .success("<b>密码修改成功</b>")
                .show(2000);

                that.$('#change-password input[type="password"]').val('');
            }
        });
        return false;
    }

})

router.register({
    name: 'dashsetting',
    dependency: [ 'dashboard' ],
    inlet: function (params, cb) {
        var that = this;

        that.view = new SettingView();
        that.view.once('render', cb);

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;

        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    },
});

