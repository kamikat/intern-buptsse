//////////////////
// Course Model //
//////////////////

var CourseModel = Model.extend({

    defaults: {
        name: '',
        department: '',
        demand: 0,
        salary: '',
        brief: '',
        mentor: {
            name: '',
            gender: '',
            position: '',
            phone: '',
            email: ''
        },
        enterprise: {
            name: '',
            website: '',
            address: '',
            lonlat: ''
        },
        task: [
            // {
            //  profile: { <Task> },
            //  <Overriden Fields>, ...
            // }
        ],
        creator: '',
        created_at: 0
    },

});

var CourseCollection = Collection.extend({

    url: '/course/',

    model: CourseModel,

});

app.course = new CourseCollection();

