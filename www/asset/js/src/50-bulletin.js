/////////////////////////////
// Administration/Bulletin //
/////////////////////////////

var AdmBulletinView = View.extend({

    events: {
        'submit form': 'onSubmit'
    },

    ui: 'administration/bulletin',

    initialize: function () {
        this.listenTo(app.environment, 'change:bulletin', this.showBulletin);
    },

    render: function() {
        this.$el.html(this.template());
        this.showBulletin();
        this.trigger('render');
        return this;
    },

    showBulletin: function () {
        this.$('textarea').val(app.environment.attributes.bulletin || "");
    },

    onSubmit: function() {
        var that = this;
        var content = that.$('textarea').val();
        $.post('/environment', { bulletin: content }, function(data) {
            app.notification.success('<b>已更新</b> 系统公告').show(2000);
            app.environment.set(data);
        });
        return false;
    }

});

router.register({
    name: 'admbulletin',
    dependency: [ 'dashadmin' ],
    inlet: function (params, cb) {
        var that = this;
        that.view = new AdmBulletinView();
        that.view.once('render', cb);

        var $viewport = that.find('dashadmin').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        return cb();
    }
});

