//////////////////
// AJAX Utility //
//////////////////

$.upload = function ($files, url, cb) {
    if (url && !cb && url instanceof Function) {
        cb = url
        url = undefined
    }

    if (!url) {
        // Root Path for User Content
        url = '/files/' + app.session.get('uid') + '/';
    }

    if (!cb) {
        cb = noop
    }

    $.ajax({
        url: url,
        files: $files,
        iframe: true,
        success: function (data) {
            if (typeof data === 'object')
                return cb (data);
            else
                return cb ({ err: 'Object too large' });
        }
    });
};

$.del = function (path, cb) {
    $.ajax({
        type: 'DELETE',
        url: path,
        success: cb
    });
};

$.ajaxSetup({

    statusCode: {
        404: function(jqXHR, textStatus, errorThrown) {
            if (app.notification) {
                var url = this.url.replace(/[?&]_=[0-9.]*/, '');
                app.notification
                .error("<b>糟糕</b> 有什么东西丢了 (((°Д°)))<br><small>" + this.type + " " + url + " 404 (Not Found)</small>")
                .show(30000);
            }
        },
        403: function(jqXHR, textStatus, errorThrown) {
            if ($.ajax.notification403)
                $.ajax.notification403.dismiss();
            if (app.notification) {
                var note = $.ajax.notification403 = app.notification
                .warn("<b>警告</b> 您的身份验证已过期，请保存您的工作后 <a href=\"#logout\">重新登录</a>")
                .show();
                note.$('a[href="#logout"]').click(function () {
                    note.dismiss(note);
                    app.session.fetch({
                        success: function () {
                            app.session.trigger('change');
                        }
                    });
                    return false;
                });
            }
        },
    },

    cache: false,
});

