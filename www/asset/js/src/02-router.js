////////////////////////
// Application Router //
////////////////////////

var AppRouter = Backbone.Router.extend({

    routes: {
        'login': 'login',
        'dashboard': 'dashboard',
        'dashboard/home': 'dashhome',
        'dashboard/course': 'dashcourse',
        'dashboard/course/:id': 'dashcourse-view',
        'dashboard/course/:id/edit': 'dashcourse-edit',
        'dashboard/task': 'dashtask',
        'dashboard/task/:id/edit': 'dashtask-edit',
        'dashboard/student': 'dashstudent',
        'dashboard/intern': 'dashintern',
        'dashboard/setting': 'dashsetting',
        'dashboard/administration' : 'dashadmin',
        'dashboard/administration/bulletin' : 'admbulletin',
        'dashboard/administration/account'  : 'admaccount',
        'dashboard/administration/account/:id/edit'  : 'admaccount-edit',
        'dashboard/administration/account/import'  : 'admaccount-import',
        'dashboard/administration/schedule' : 'admschedule',
        'dashboard/administration/faq'      : 'admfaq',
        'dashboard/faq': 'dashfaq',
        'activation': 'activation',
        'logout': 'logout',
    },

    phantom: {},

    initialize: function () {
        var that = this;

        Backbone.history.on('loadurl', that.handleUrl, that);
        that.move = setImmediate.bind(that, that.move.bind(that));
    },

    register: function (phantom) {
        var that = this;

        phantom = _.extend({
            dependency: [],     // Specify dependency on other route controller
            inlet: noop,        // Factory method called to initialize the route
            outlet: noop,       // Factory method called to destroy the route
            shift: noop         // Factory method called when the route parameter changes
        }, phantom);

        // find phantom
        phantom.find = function (phantom_name) {
            return that.phantom[phantom_name];
        };

        that.phantom[phantom.name] = bindthat(phantom, phantom);
    },

    eject: function (route, params, cb) {
        var that = this;

        if (!route) return cb();

        var factory = that.phantom[route];

        if (factory && factory.outlet) {
            console.debug("Eject '" + route + "'");
            factory.outlet(params, cb);
        } else {
            console.warn("Cannot find route outlet: '" + route + "'");
            return cb();
        }
    },

    inject: function (route, params, cb) {
        var that = this;

        if (!route) return cb();

        var factory = that.phantom[route];

        if (factory && factory.inlet) {
            console.debug("Inject '" + route + "'");
            factory.inlet(params, cb);
        } else {
            console.warn("Cannot find route inlet: '" + route + "'");
            return cb();
        }
    },

    shift: function (route, params, cb) {
        var that = this;

        if (!route) return cb();

        var factory = that.phantom[route];

        if (factory && factory.shift) {
            console.debug("Shift '" + route + "'");
            factory.shift(params, cb);
        } else {
            console.warn("Cannot find route shift: '" + route + "'");
            return cb();
        }
    },

    calcDependency: function (route) {
        var that = this;

        var gin = {};

        function _deptrack (route) {
            var factory = that.phantom[route];

            if (!factory) {
                console.warn("Cannot find route dependency: '" + route + "'");
                return;
            }

            var deps = factory.dependency;

            // Construct adjtable for in-degree information (inlet)
            if (!gin.hasOwnProperty(route)) {
                gin[route] = deps;
                deps.forEach(_deptrack);
            }
        }

        _deptrack(route);

        return gin;
    },

    controlnumber: 0,

    // To inject
    // (in-graph)
    workstack: {
        // <routename> : on     : [<dependency>, ...]
        //               by     : [<children>, ...]
        //               params : [<params>]
        //               state  :  <state>
    },

    // Injected or on-the-fly (injecting, ejecting) controllers
    // (out-graph)
    controller: {
        // <routename> : on     : [<dependency>, ...]
        //               by     : [<children>, ...]
        //               params : [<params>]
        //               state  :  <state>
    },

    // Strategy for Inject/Eject:
    //  1. Parse dependency graph for target, send it to workstack.
    //  2. Mark all controllers of target, attach with params.
    //  3. For all reusable controller, mark as 'reuse'.
    //  4. Kick-on I/E worker.
    //
    // Logic of worker:
    //  1. Look at controller, find garbages with out-degree=0.
    //     Goto 4 if all garbages are clear.
    //  2. For each garbage, determine their state (loading or loaded)
    //      a. If ready(loaded), eject it, mark it as ejecting.
    //      b. If injecting, skip eject. (will goto 2a after callback of injection)
    //      c. If ejecting, skip eject. (will goto 2d after callback of ejection)
    //      d. If not exist, garbage collected
    //  3. After each callback, kick I/E worker (Goto 1).
    //  4. Look at workstack, find controllers with in-degree=0.
    //     Routing finished if all controllers are loaded.
    //  5. For each controller, determine their state in controller graph.
    //      a. If not exist, inject it, mark it as injecting.
    //      b. If ejecting, skip inject. (will goto 5a after callback of ejection)
    //      c. If injecting, skip inject. (will goto 5d after callback of injection)
    //      d. If ready(loaded), skip inject.
    //      e. If reuse, call shift, mark status as injecting.
    //  6. After each callback, kick I/E worker (Goto 1)

    move: function () {
        var that = this;

        var neat = true;

        _(that.controller).each(function (ctrl, route) {
            if (ctrl.cn >= that.controlnumber) {
                return;
            }
            ctrl.by = _(ctrl.by).filter(function (x) {
                return that.controller.hasOwnProperty(x);
            });
            if (ctrl.by.length == 0) {
                // Nothing depend on the controller any more
                if (ctrl.state == 'ready' || ctrl.state == 'reuse') {
                    ctrl.state = 'ejecting';
                    that.eject(route, ctrl.params, function () {
                        ctrl.on.forEach(function (x) {
                            that.controller[x].by.removeItem(route);
                        });
                        delete that.controller[route];
                        that.move();
                    });
                }
            }
            neat = false;
        });

        if (!neat) return;

        _(that.workstack).each(function (target, route) {
            if (_(target.on).any(function (x) {
                return !that.controller[x] || that.controller[x].state != 'ready';
            })) {
                return;
            }

            // The controller's dependency has been all loaded
            var ctrl = that.controller[route];

            if (!ctrl) {
                that.controller[route] = ctrl = target;
                ctrl.state = 'injecting';
                ctrl.on.forEach(function (x) {
                    if (that.controller[x].by.indexOf(route) == -1) {
                        that.controller[x].by.push(route);
                    }
                });
                that.inject(route, target.params, function () {
                    if (ctrl.state == 'injecting') {
                        ctrl.state = 'ready';
                    } else if (ctrl.state == 'injecting-to-reuse') {
                        ctrl.state = 'reuse';
                    }
                    that.move();
                });
            } else if (ctrl.state == 'reuse') {
                ctrl.state = 'injecting';
                that.shift(route, target.params, function () {
                    ctrl.state = 'ready';
                    that.move();
                });
            }
        });
    },

    handleUrl: function (fragment) {
        var that = this;

        // If fragment changed
        if (that.lastfrag != fragment) {
            that.controlnumber++;

            // Determine route & params for current fragment
            var r = that.translate(fragment);

            // Resolve target dependencies
            var deps = that.calcDependency(r.route);

            var workstack = {};

            for (var key in deps) {
                workstack[key] = _({}).extend({
                    on: deps[key],
                    by: [],
                    cn: that.controlnumber,
                    params: r.params
                });
                if (that.controller.hasOwnProperty(key)) {
                    that.controller[key].cn = that.controlnumber;
                    if (that.controller[key].state == 'ready') {
                        that.controller[key].state = 'reuse';
                    } else if (that.controller[key].state == 'injecting') {
                        that.controller[key].state = 'injecting-to-reuse';
                    }
                }
            }

            that.workstack = workstack;

            that.move();

            console.debug("Navigating from '" + that.lastfrag + "' to '" + fragment + "'");

            // Update last fragment
            that.lastfrag = fragment;
        }
    },

    // Get current route
    current: function () {
        return this.translate(Backbone.history.fragment);
    },

    subroute: function () {
        return this._subroute(Backbone.history.fragment);
    },

    _subroute: function (fragment) {
        var routes = [];
        var g = fragment.split('/');
        for (var i = 1; i <= g.length; i++) {
            routes.push(g.slice(0, i).join('/'));
        }
        return routes;
    },

    translate: function (fragment) {
        var that = this;
        var routes = _.pairs(that.routes);

        var route = null;
        var params = null;

        var matched = _.find(routes, function(handler) {
            route = _.isRegExp(handler[0]) ? handler[0] : that._routeToRegExp(handler[0]);
            return route.test(fragment);
        });

        if (matched) {
            // NEW: Extracts the params using the internal
            // function _extractParameters
            params = that._extractParameters(route, fragment);
            route = matched[1];
        } else {
            route = undefined;
        }

        return {
            route : route,
            fragment : fragment,
            params : params
        };
    }

});

var router = new AppRouter();




