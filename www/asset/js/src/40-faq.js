///////////////////
// Dashboard/FAQ //
///////////////////

var FaqView = View.extend({

    ui: 'dashboard/faq',

});

router.register({
    name: 'dashfaq',
    dependency: [ 'dashboard' ],
    inlet: function (params, cb) {
        var that = this;
        that.view = new FaqView();

        var loadtext = (function () {
            var faqtext = "";

            $.get('/faq.md', function (html) {
                faqtext = html;
                loadtext();
            });

            return function () {
                if (that.view) {
                    that.view.$('.white-well').html(faqtext);
                }
            };
        })();

        that.view.once('render', function () {
            loadtext();
            return cb();
        });

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        return cb();
    },
});

