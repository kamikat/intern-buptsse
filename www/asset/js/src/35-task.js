////////////////////
// Dashboard/Task //
////////////////////

var TaskView = View.extend({

    events: {

    },

    ui: 'dashboard/task',

    initialize: function () {

        // XXX Not Efficient

        var that = this;

        that.listenTo(that.collection, 'request', function handleRequest () {
            that.stopListening(that.collection);
            that.listenTo(that.collection, 'sync', function () {
                that.render();
                that.listenTo(that.collection, 'add', that.render);
                that.listenTo(that.collection, 'change', that.render);
                that.listenTo(that.collection, 'remove', that.render);
                that.listenTo(that.collection, 'reset', that.render);
            });
            that.listenTo(that.collection, 'request', handleRequest);
        });
    },

    render: function () {
        var that = this;

        that.$el.html(that.template());

        that.collection.each(function (task) {
            var view = new TaskItemView({
                model: task
            });
            that.$('tbody').append(view.render().el);
        });

        that.trigger('render');

        return that;
    },

});

var TaskItemView = View.extend({

    tagName: 'tr',

    events: {
        'click': 'edit'
    },

    ui: 'dashboard/task-item',

    edit: function () {
        router.navigate(
            'dashboard/task/' + this.model.id + '/edit',
            { trigger: true }
        );
    },

});

router.register({
    name: 'dashtask',
    dependency: [ 'dashboard' ],
    inlet: function (param, cb) {
        var that = this;

        that.view = new TaskView({
            collection: app.task
        });

        app.task.fetch();

        that.view.once('render', cb);

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (param, cb) {
        var that = this;

        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    }
});

