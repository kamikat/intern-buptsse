////////////////////////
// Constructing Alert //
////////////////////////

var ConstructingAlertView = View.extend({

    ui: 'constructing',

});

router.register({
    name: 'constructing',
    dependency: [ 'dashboard' ],
    inlet: function (params, cb) {
        var that = this;
        that.view = new ConstructingAlertView();
        that.view.once('render', cb);

        var $viewport = that.find('dashboard').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        return cb();
    }
});
