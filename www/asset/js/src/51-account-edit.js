/////////////////////////////////
// Administration/Account/Edit //
/////////////////////////////////

var AdmAccountEditorView = View.extend({

    events: {
        'submit form': 'save',
        'click .btn-delete': 'showRemoveConfirm',
        'click .btn-cancel-delete, a.close': 'dismissRemoveConfirm',
        'click .btn-confirm-delete': 'deleteItem',
        'click .btn-reset-password': 'showPasswordInput',
        'click .btn-reset-cancel': 'hidePasswordInput',
    },

    ui: 'administration/account-editor',

    target: function (account) {

        if (this.model)
            this.stopListening(this.model);

        this.model = account;

        // Reset the form

        this.$('.btn-group').children().removeClass('active');
        this.$('form')[0].reset();
        this.dismissRemoveConfirm();

        if (account) {

            this.$('input[name="username"]').disabled(true);
            this.$('.btn-delete').removeClass('hide');

            this.$('.btn-reset-password').show();
            this.$('.fieldset-reset-password').hide();

            this.listenTo(this.model, 'change', this.target.bind(this, this.model));

            // Fill in existing values

            this.$('input[name="username"]').val(account.get('username'));
            var group = account.get('group');

            this.$('.radio-group button[data-val="' + group + '"]')
            .addClass('active');

            // Limit the selection of radio-group
            this.$('.radio-group button[data-val!="admin"]').disabled(group == 'admin');
            this.$('.radio-group button[data-val="admin"]').disabled(group != 'admin');
            this.$('.btn-reset-password').disabled(group == 'admin');
            this.$('.btn-delete').disabled(group == 'admin');

            this.$('input[name="name"]').val(account.get('profile').name);
            this.$('input[name="phone"]').val(account.get('profile').phone);
            this.$('input[name="email"]').val(account.get('profile').email);

        } else {

            this.$('input[name="username"]').disabled(false);
            this.$('.btn-reset-password').hide();
            this.$('.btn-reset-cancel').hide();
            this.$('.fieldset-reset-password').show().removeClass('input-append')
            .find('input[name="password"]').val(randomString(10));
            this.$('.btn-delete').addClass('hide');

        }
    },

    showPasswordInput: function () {
        this.$('.btn-reset-password').hide();
        this.$('.btn-reset-cancel').show();
        this.$('.fieldset-reset-password').show().addClass('input-append')
        .find('input[name="password"]').val(randomString(10));
    },

    hidePasswordInput: function () {
        this.$('.btn-reset-password').show();
        this.$('.fieldset-reset-password').hide()
        .find('input[name="password"]').val('');
    },

    save: function () {
        var that = this;
        var account = that.model;
        var group = that.$('.radio-group button.active').data('val');

        if (account) {

            var _account = { };

            if ($('.fieldset-reset-password').is(':visible')) {
                _account.password = md5(that.$('input[name="password"]').val());
            }

            if (group != account.get('group')) {
                _account.group = group;
            }

            if (_.size(_account)) {
                $.post(account.url(), _account, function (data) {
                    if (data.err) {
                        app.notification
                        .error("<b>失败</b> 无法更改用户账户设置")
                        .show(5000);
                    } else {
                        app.notification
                        .success("<b>已更新</b> 用户账户设置")
                        .show(3000);

                        // Hide password reset input after update
                        that.hidePasswordInput();

                        account.set(data);
                    }
                });
            }

            var profile = {
                name: that.$('input[name="name"]').val(),
                phone: that.$('input[name="phone"]').val(),
                email: that.$('input[name="email"]').val()
            };

            var patch = _.patch(account.get('profile'), profile);

            if (_.size(patch)) {
                $.post('/u/' + account.id + '/profile', patch, function (data) {
                    if (data.err) {
                        app.notification
                        .error("<b>失败</b> 无法更新用户 " + account.get('username') + " 的用户资料")
                        .show(5000);
                    } else {
                        app.notification
                        .success("<b>已更新</b> 用户 " + account.get('username') + "的用户资料")
                        .show(3000);
                        var _profile = _(account.get('profile')).chain()
                        .clone().extend(data)
                        .value();
                        account.set('profile', _profile);
                    }
                });
            }

        } else {

            var dowith = function (next) {
                next.shift()(null, next);
            };

            var createAccount = function (params, next) {
                $.post('/account/', {
                    username: that.$('input[name="username"]').val(),
                    password: md5(that.$('input[name="password"]').val()),
                    group: group
                }, function (data) {
                    if (data.err) {
                        app.notification
                        .error("<b>失败</b> 无法创建用户账户")
                        .show(5000);
                    } else {
                        app.notification
                        .success("<b>已创建</b> 用户账户 " + data.uid)
                        .show(3000);
                        return next.shift()(data, next);
                    }
                });
            };

            var updateProfile = function (params, next) {
                var profile = {
                    name: that.$('input[name="name"]').val(),
                    phone: that.$('input[name="phone"]').val(),
                    email: that.$('input[name="email"]').val()
                };

                $.post('/u/' + params.uid + '/profile', profile, function (data) {
                    if (data.err) {
                        app.notification
                        .error("<b>失败</b> 无法设置用户 " + params.uid + " 的用户资料")
                        .show(5000);
                    } else {
                        app.notification
                        .success("<b>已更新</b> 用户 " + params.uid + "的用户资料")
                        .show(3000);
                        return next.shift()(params, next);
                    }
                });
            };

            var syncData = function (params, next) {
                app.accounts.fetch({
                    success: function () {
                        router.navigate(
                            'dashboard/administration/account/' + params.uid + '/edit',
                            { trigger: true, replace: true }
                        );
                    }
                });
            };

            dowith([createAccount, updateProfile, syncData]);

        }

        return false;
    },

    showRemoveConfirm: function () {
        this.$('.note-delete').removeClass('hide');
    },

    dismissRemoveConfirm: function () {
        this.$('.note-delete').addClass('hide');
    },

    deleteItem: function () {
        var account = this.model;

        if (account) {
            $.del(account.url(), function (data) {
                if (data.err) {
                    app.notification
                    .error("<b>失败</b> 无法删除用户账户")
                    .show(3000);
                } else {
                    app.notification
                    .success("<b>已删除</b> 用户账户 " + account.id)
                    .show(3000);
                    app.accounts.fetch({
                        success: function () {
                            router.navigate(
                                'dashboard/administration/account',
                                { trigger: true, replace: true }
                            );
                        }
                    });
                }
            });
        }

        this.dismissRemoveConfirm();
    },

});

router.register({
    name: 'admaccount-edit',
    dependency: [ 'admaccount' ],
    inlet: function (param, cb) {
        var that = this;

        that.view = new AdmAccountEditorView();
        that.view.once('render', function () {
            that.shift([ param.shift() ], cb);
        });

        var $viewport = that.find('dashadmin').$viewport;
        that.view.render().$el.appendTo($viewport);

        that.find('admaccount').view.$el.hide(); // Hide account view
    },
    outlet: function (param, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        that.find('admaccount').view.$el.show(); // Show account view
        return cb();
    },
    shift: function (param, cb) {
        var that = this;
        var id = param.shift();

        if (id == '+') {
            // Create new item
            that.view.target();
        } else {
            // Edit existing item
            that.view.target(app.accounts.get(id));
        }

        return cb();
    }
});

