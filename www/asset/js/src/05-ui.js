////////////////////////
// UI Template Loader //
////////////////////////

var ui = function (view, template) {

    var cached = uirequest(template);

    if (cached.__cache__ != 'pending') {
        return view.template = cached;
    }

    var _render = view.render.bind(view);
    var pending = false;

    view.template = noop;
    view.render = function () {
        if (!ready) {
            pending = true;
        } else {
            _render.apply(arguments);
        }
        return this;
    }

    var _remove = view.remove;

    view.remove = function () {
        view.render = noop;
        pending = false;
        _remove.bind(view)();
    };

    var ready = false;

    // Append a callback (`template' should be a function)
    cached.callback.push(function (template) {
        ready = true;
        view.template = template;
        // view.render = _render;
        if (pending) {
            _render();
        }
    });
};

var uirequest = function (template, callback) {
    var path = '/ui/' + template + '.js';
    return cache(path, function (obj) {
        // Compile code to template function
        if ('string' == typeid(obj)) {
            var code = obj;
            cache.cache[path] = (function (template) {
                return function (args) {
                    // export `util' to jade context
                    var data = args || {};
                    data.util = util;
                    return template(data);
                };
            }(eval(code)));
        }
    });
};

