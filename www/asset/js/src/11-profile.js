///////////////////
// Profile Model //
///////////////////

var ProfileModel = Model.extend({

    defaults: {
        active: false,
        name: '',
        phone: '',
        email: '',
        resume: ''
    },

    initialize: function () {
        var that = this;

        that.on('change:email', function (model, email) {
            if (email) {
                that.set('photo', "//en.gravatar.com/avatar/" + md5(email) + "?d=identicon");
            }
        });
    },

});

app.profile = new ProfileModel();

app.session.on('change', function () {
    if (app.session.get('authenticated')) {
        fetch.profile(app.session.get('uid'), app.profile);
    } else {
        app.profile.clear({ silent: true });

        // XXX delete id
        app.profile.id = undefined;
    }
});

app.profile.on('change', function () {
    if (!app.profile.get('active')) {
        router.navigate('activation', { trigger: true });
    } else {
        if (['activation'].indexOf(router.current().route) != -1) {
            return router.navigate('dashboard', { trigger: true, replace: true });
        }
    }
});

