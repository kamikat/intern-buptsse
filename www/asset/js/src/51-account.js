////////////////////////////
// Administration/Account //
////////////////////////////

var AdmAccountModel = Model.extend({

    idAttribute: 'uid',

});

var AdmAccountCollection = Collection.extend({

    url: '/account/',

    model: AdmAccountModel,

});

app.accounts = new AdmAccountCollection();

var AdmAccountView = View.extend({

    events: { },

    ui: 'administration/account',

    initialize: function () {

        // XXX Not Efficient

        var that = this;

        that.listenTo(that.collection, 'request', function handleRequest () {
            that.stopListening(that.collection);
            that.listenTo(that.collection, 'sync', function () {
                that.render();
                that.listenTo(that.collection, 'add', that.render);
                that.listenTo(that.collection, 'change', that.render);
                that.listenTo(that.collection, 'remove', that.render);
                that.listenTo(that.collection, 'reset', that.render);
            });
            that.listenTo(that.collection, 'request', handleRequest);
        });
    },

    render: function () {
        var that = this;

        that.$el.html(that.template());

        that.collection.each(function (account) {
            var view = new AdmAccountItemView({
                model: account
            });
            that.$('tbody').append(view.render().el);
        });

        that.trigger('render');

        return that;
    },

});

var AdmAccountItemView = View.extend({

    tagName: 'tr',

    events: {
        'click': 'edit'
    },

    ui: 'administration/account-item',

    edit: function () {
        router.navigate(
            'dashboard/administration/account/' + this.model.id + '/edit',
            { trigger: true }
        );
    },

});

router.register({
    name: 'admaccount',
    dependency: [ 'dashadmin' ],
    inlet: function (param, cb) {
        var that = this;
        that.view = new AdmAccountView({
            collection: app.accounts
        });
        app.accounts.fetch();
        that.view.once('render', cb);

        var $viewport = that.find('dashadmin').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (param, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        return cb();
    }
});

