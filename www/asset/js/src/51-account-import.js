///////////////////////////////////
// Administration/Account/Import //
///////////////////////////////////

var AdmAccountImportView = View.extend({

    events: {
        'submit form': 'submit'
    },

    ui: 'administration/account-import',

    submit: function () {
        var that = this;

        if (that.$(':file[name="import"]').val()) {
            var uploadingnote = app.notification
            .info("<b>正在上传</b> 用户数据文件")
            .show(10000);

            that.$('.import-report').addClass('hide');
            that.$('.import-report .error-info').addClass('hide');

            $.upload(that.$(':file[name="import"]'),
                     '/account/!import',
                     function (data) {
                uploadingnote.dismiss();
                if (data.err) {
                    app.notification
                    .error("<b>导入失败</b> 请检查文件格式是否正确")
                    .show(5000);
                } else {

                    if (data.created > 0) {
                        app.notification
                        .success("<b>导入成功</b> 已创建 " + data.created + " 新用户")
                        .show(3000);
                    }

                    that.$('.import-report').removeClass('hide');
                    that.$('.report-field-count').text(data.count);
                    that.$('.report-field-created').text(data.created);

                    if (data.created != data.count) {
                        app.notification
                        .warn("<b>注意</b> 有部分用户无法创建, 请核对日志信息后对部分用户尝试重新导入")
                        .show(5000);

                        uirequest('administration/account-import-log', function (template) {
                            var $el = that.$('.import-report .error-info tbody');
                            $el.empty();
                            _(data.failed).each(function (item) {
                                $(template(item)).appendTo($el);
                            });
                            that.$('.import-report .error-info').removeClass('hide');
                        });
                    }
                }
                that.$('.fileupload').fileupload('clear').removeData('fileupload').fileupload();
                app.accounts.fetch();
            });
        } else {
            app.notification
            .error("<b>错误</b> 请选择需要上传的文件")
            .show(2000);
        }

        return false;
    },

});

router.register({
    name: 'admaccount-import',
    dependency: [ 'admaccount' ],
    inlet: function (param, cb) {
        var that = this;
        that.view = new AdmAccountImportView();
        that.view.once('render', cb);

        var $viewport = that.find('dashadmin').$viewport;
        that.view.render().$el.appendTo($viewport);

        that.find('admaccount').view.$el.hide(); // Hide account view
    },
    outlet: function (param, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        that.find('admaccount').view.$el.show(); // Show account view
        return cb();
    }
});

