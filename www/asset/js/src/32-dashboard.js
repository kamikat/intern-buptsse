///////////////
// Dashboard //
///////////////

var DashboardView = View.extend({

    ui: 'dashboard',

    initialize: function () {
        this.listenTo(app.profile, 'change', this.showProfile);
        this.listenTo(router, 'route', this.showTab);
        this.listenTo(app.environment, 'change:bulletin', this.showBulletin);
    },

    render: function () {
        this.$el.html(this.template());
        this.showProfile();
        this.showTab();
        this.showBulletin();
        this.trigger('render');
        return this;
    },

    showBulletin: function () {
        this.$('.bulletin-text').text(app.environment.attributes.bulletin || "系统暂无公告");
    },

    showProfile: function () {
        var that = this;
        that.$('#profile-name').text(app.profile.get('name') || '/');
        that.$('#profile-id').text(app.profile.id);
        that.$('#profile-phone').text(app.profile.get('phone') || '/');
        that.$('#profile-email').text(app.profile.get('email') || '/');
        that.$('#profile-photo').attr('src', app.profile.get('photo'));
    },

    showTab: function () {
        this.$('.nav li').removeClass('active');
        var selector = _.map(router.subroute(), function (x) {
            return '.nav li a[href="#' + x + '"]';
        });
        this.$(selector.join(', ')).parent().addClass('active');
    },

});

router.register({
    name: 'dashboard',
    inlet: function (params, cb) {
        var that = this;

        that.view = new DashboardView();
        that.view.once('render', function () {
            that.$viewport = that.view.$('#dashboard-content');
            return cb();
        });

        var $viewport = app.$rootview;
        that.view.render().$el.appendTo($viewport);

        // Check activation
        if (app.profile.id) {
            app.profile.trigger('change');
        }

        this.shift(params, noop);
    },
    outlet: function (params, cb) {
        var that = this;

        // Unload the dashhome view
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    },
    shift: function (params, cb) {
        if (Backbone.history.fragment == 'dashboard') {
            router.navigate('dashboard/home', { trigger: true, replace: true });
        }
        return cb();
    }
});

