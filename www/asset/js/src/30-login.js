///////////
// Login //
///////////

var LoginView = View.extend({

    events: {
        'submit form': 'login'
    },

    ui: 'login',

    login: function () {
        var that = this;

        if (that.notification) {
            (function (notification) {
                setTimeout(function () {
                    notification.dismiss();
                }, 200);
            })(that.notification);
            that.notification = undefined;
        }

        var notelogining = app.notification
        .info("用户登录中...")
        .show(10000);

        var username = this.$('.username input').val();
        var password = this.$('.password input').val();

        cd.component.session.login(username, password, function (data) {
            notelogining.dismiss();

            if (data.err) {
                // Show error popup
                that.notification = app.notification
                .error("<b>登录失败</b> 用户名或密码错误")
                .show(10000);
            } else {
                app.notification
                .success("<b>登录成功</b> 正在加载页面, 请稍候")
                .show(2000);
            }
        });
        return false;
    }

});

router.register({
    name: 'login',
    inlet: function (params, cb) {
        var that = this;

        // Load login view
        that.view = new LoginView();
        that.view.once('render', cb);
        that.view.render().$el.appendTo(app.$rootview);

        // Check if user has been logged in
        app.session.trigger('change');

    },
    outlet: function (params, cb) {
        var that = this;

        // Unload the login view
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }

        return cb();
    }
});

