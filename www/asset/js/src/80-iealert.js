////////////////
// IE Warning //
////////////////

var CompatibilityAlertView = View.extend({

    events: {
        'click button': 'close',
    },

    ui: 'upgrade-alert',

    close: function () {
        this.remove();
        app.session.fetch();
    },

});

