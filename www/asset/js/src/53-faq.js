////////////////////////
// Administration/FAQ //
////////////////////////

var AdmFaqView = View.extend({

    events: {
        'submit form': 'onSubmit'
    },

    ui: 'administration/faq',

    onSubmit: function() {
        var that = this;
        var content = that.$('textarea').val();
        $.ajax({
            type: 'POST',
            url: '/faq',
            headers: {
                'Content-Type': 'text/plain'
            },
            data: content,
            success: function(data) {
                app.notification.success('<b>已更新</b> 常见问题').show(2000);
            }
        });
        return false;
    }

});

router.register({
    name: 'admfaq',
    dependency: [ 'dashadmin' ],
    inlet: function (params, cb) {
        var that = this;
        that.view = new AdmFaqView();

        var loadtext = (function () {
            var faqtext = "";

            $.get('/faq', function (data) {
                faqtext = data;
                loadtext();
            });

            return function () {
                if (that.view) {
                    that.view.$('textarea').val(faqtext);
                }
            };
        })();

        that.view.once('render', function () {
            loadtext();
            return cb();
        });

        var $viewport = that.find('dashadmin').$viewport;
        that.view.render().$el.appendTo($viewport);
    },
    outlet: function (params, cb) {
        var that = this;
        if (that.view) {
            that.view.remove();
            that.view.unbind();
            that.view = undefined;
        }
        return cb();
    }
});

